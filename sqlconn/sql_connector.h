#ifndef SQL_CONN_H
#define SQL_CONN_H

#if defined(_WIN32) || defined(__WIN32__)
#	include <windows.h>
#endif
#include <stdio.h>
#include <unistd.h>
#include <mysql/mysql.h>
#include <list>
#include "../ui/qt/ScoreRecord.h"

#define CONN_CONFIG_FILE "conn_config"

#ifndef byte
#	define byte        unsigned char
#endif


struct conn_data
{
    const char * server_name;
    const char * user_name;
    const char * db_name;
    const char * db_passwd;
    int db_port;
    int db_cols;
    char ** table_data; // name col1 col2 col3 ...
};

class Sql_connector
{
    public:
        
        struct conn_data * c_data;
        MYSQL * conn;
    
        Sql_connector(void);
        ~Sql_connector(void);
    
        int connect(void);
        int disconnect(void);
        int insertScore(const char * name,int score,byte difficulty);
        /** Loads top 10 results (of given level) from database.
         * @param difficulty Game level for which to load data.
         * @param recList List to store results.
         * @return True on success, false if loading fails. */
        bool getTop10(byte difficulty, std::list<ScoreRecord> &recList);
        void writeConfig(void);
        void loadConfig(void);
    
};

#endif
