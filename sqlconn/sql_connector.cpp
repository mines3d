#include "sql_connector.h"
#include <time.h>
#include <stdlib.h>
#include <cstring>
#include <sstream>

#define TABLEDATA_BUFSIZE       255

#define TABLE_NAME_I            0
#define TABLE_COL_NAME_I        1
#define TABLE_COL_SCORE_I       2
#define TABLE_COL_DATE_I        3
#define TABLE_COL_DIFFICULTY_I  4

Sql_connector::Sql_connector(void)
{
    int i;
    
    c_data = ( struct conn_data * )malloc( sizeof(struct conn_data) );
    c_data->server_name = "webdev.felk.cvut.cz";
    c_data->user_name = "horkyja3";
    c_data->db_name = "horkyja3";
    c_data->db_passwd = "tq4ovxdu";
    c_data->db_port = 3306;
    c_data->db_cols = 4;
    
    if( (c_data->table_data = (char**)malloc((c_data->db_cols+1) * sizeof *(c_data->table_data) )) )
    {
        for (i=0;i<=c_data->db_cols;i++)
        {
            c_data->table_data[i] = (char *)malloc( sizeof(char) * TABLEDATA_BUFSIZE+1 );
        }
        
        snprintf(c_data->table_data[TABLE_NAME_I],           TABLEDATA_BUFSIZE, "api_miny_score");
        snprintf(c_data->table_data[TABLE_COL_NAME_I],       TABLEDATA_BUFSIZE, "jmeno");
        snprintf(c_data->table_data[TABLE_COL_SCORE_I],      TABLEDATA_BUFSIZE, "skore");
        snprintf(c_data->table_data[TABLE_COL_DATE_I],       TABLEDATA_BUFSIZE, "date");
        snprintf(c_data->table_data[TABLE_COL_DIFFICULTY_I], TABLEDATA_BUFSIZE, "difficulty");
        
        fprintf(stderr, "malloc ok\n");
  
    }
    else
    {
        fprintf(stderr, "malloc failed\n");
    }
    
    
}

Sql_connector::~Sql_connector(void)
{
    int i;
    
    for (i=0;i<=c_data->db_cols;i++)
    {
        free(c_data->table_data[i]);
    }
    free( c_data->table_data );
    free(c_data);
    
}

int Sql_connector::connect(void)
{
    fprintf(stderr, "MySQL client version: %s\n", mysql_get_client_info());
  
    conn = mysql_init(0);
  
    if (conn == 0) {
        fprintf(stderr, "error: %u %s\r\n",mysql_errno(conn),mysql_error(conn));
    }
    
    if (mysql_real_connect(conn,c_data->server_name,c_data->user_name,
        c_data->db_passwd,c_data->db_name,c_data->db_port,0,0) == 0 )
    {
        fprintf(stderr, "error: %u %s\r\n",mysql_errno(conn),mysql_error(conn)); 
    }
    fprintf(stderr, "mysql connect ok\n");
    return 0;
}

int Sql_connector::disconnect(void)
{
    mysql_close(conn);
    fprintf(stderr, "disconnected.\n");
    return 0;
}

int Sql_connector::insertScore(const char * name, int score,byte difficulty)
{
    char * query;
    int len = 0;
    int i;
    
    time_t t;
    tm * ptm;
    time(&t);
    
    ptm = gmtime ( &t );
    ptm->tm_year += 1900;
    ptm->tm_mon++;
    (++ptm->tm_hour) %= 24;
    
    for (i=0;i<=c_data->db_cols;i++)
    {
        len += strlen( c_data->table_data[i]);
    }
    
    len += 100;
    query = (char * ) malloc(sizeof(char) * len);
    fprintf(stderr, "%i\n",ptm->tm_isdst);
    sprintf(query,"INSERT INTO %s (%s,%s,%s,%s) VALUES ( '%s',%i,'%i-%i-%i %i:%i:%i','%i' );",
        c_data->table_data[0],c_data->table_data[1],c_data->table_data[2],c_data->table_data[3],c_data->table_data[4],
        name,score,ptm->tm_year,ptm->tm_mon,ptm->tm_mday,ptm->tm_hour,ptm->tm_min,ptm->tm_sec,difficulty); 
    if( (len = mysql_query(conn,query)) )
    {
        fprintf(stderr, "error: %u %s\r\n",mysql_errno(conn),mysql_error(conn)); 
    }
    
    free(query);
    
    return len;
}

bool Sql_connector::getTop10(byte difficulty, std::list<ScoreRecord> &recList) {
	std::ostringstream os;
	MYSQL_RES *sqlResult;
	MYSQL_ROW sqlRow;
	ScoreRecord scoreItem;
	
	//e.g. "SELECT jmeno,skore FROM api_miny_score WHERE difficulty=0 ORDER BY skore ASC LIMIT 0,10"
	os << "SELECT " << c_data->table_data[TABLE_COL_NAME_I] << "," <<
			c_data->table_data[TABLE_COL_SCORE_I] << " FROM " << c_data->table_data[TABLE_NAME_I] <<
			" WHERE " << c_data->table_data[TABLE_COL_DIFFICULTY_I] << "=" << (int)difficulty <<
			" ORDER BY " << c_data->table_data[TABLE_COL_SCORE_I] << " ASC LIMIT 0,10;";

	if(mysql_query(conn, os.str().c_str())) {
		fprintf(stderr, "error: %u %s\r\n",mysql_errno(conn),mysql_error(conn));
		return false;
	}
	sqlResult = mysql_use_result(conn);
	
	recList.clear();
	while((sqlRow = mysql_fetch_row(sqlResult))) {
		strncpy(scoreItem.name, sqlRow[0], sizeof(scoreItem.name));
		scoreItem.name[sizeof(scoreItem.name)]='\0';
		scoreItem.time = (int)strtol(sqlRow[1], NULL, 10);
		recList.push_back(scoreItem);
	}

	mysql_free_result(sqlResult);
	return true;
}
