/* 
 * File:   CAudio.h
 * Author: Jindrich Kovar
 * Uses OpenAL (alut.h, alc.h, al.h)
 */
#ifndef __CAudio_h_
#define __CAudio_h_

#include <AL/alut.h>
#include <stdio.h>
#include "../exceptions/CAudioException.h"

#define NUM_SOURCES      6
#define NUM_BUFFERS      6
#define NUM_MUSICS       2
#define SOUND_NOTHING   -1
#define MUSIC_NOTHING   -1

#define MUSIC_SLOW_SOURCE           "sound/soundfiles/slow_music.wav"
#define MUSIC_FAST_SOURCE           "sound/soundfiles/fast_music.wav"
#define SOUND_BUTTON_SUCCESS_SOURCE "sound/soundfiles/chain_clink.wav"
#define SOUND_BUTTON_HOVER_SOURCE   "sound/soundfiles/click.wav"
#define SOUND_GAME_OVER_SOURCE      "sound/soundfiles/sniper_rifle.wav"
#define SOUND_GAME_VICTORY_SOURCE   "sound/soundfiles/musicbox.wav"

#define MUSIC_SLOW                  0
#define MUSIC_FAST                  1
#define SOUND_BUTTON_SUCCESS        2
#define SOUND_BUTTON_HOVER          3
#define SOUND_GAME_OVER             4
#define SOUND_GAME_VICTORY          5
class CAudio
{
      public:
             CAudio();
             ~CAudio();
             void PlayMusic(int id);
             void StopMusic(void);
             void PlayEffect(int id);
             void StopEffect(void);
             
             bool bSoundEnabled;
      private:            
              ALuint Buffers[NUM_BUFFERS];         // Buffers hold sound data.              
              ALuint Sources[NUM_SOURCES];         // Sources are points of emitting sound.            
              ALfloat SourcesPos[NUM_SOURCES][3];  // Position of the source sounds.              
              ALfloat SourcesVel[NUM_SOURCES][3];  // Velocity of the source sounds.
              ALfloat ListenerPos[3] ;// Position of the listener.
              ALfloat ListenerVel[3] ;// Velocity of the listener.
              ALfloat ListenerOri[6] ;// Orientation of the listener. (first 3 elements are "at", second 3 are "up")
              int iActuallyPlayingMusic;
              int iActuallyPlayingSound;
};
#endif
