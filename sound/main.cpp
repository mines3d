//#include <cstdlib>
#include <iostream>
#include <conio.h>

#include "CAudio.h"

using namespace std;

int main(int argc, char *argv[])
{
    cout<<"Audio test"<<endl;
    bool bNoError=true;
    
    CAudio *Audio=NULL;
    try 
    {       
        Audio=new CAudio();
    } 
    catch (GeneralException e)
          {
              cout<<e.errText<<endl;
              bNoError=false;
          }

    char c;
    while (c != 'q')
    {
        c = getche();
        switch (c)
        {
               case '0':
                    if (bNoError)Audio->PlayMusic(MUSIC_SLOW);
                    break;
               case '1':
                    if (bNoError)Audio->PlayMusic(MUSIC_FAST);
                    break;
               case '2':
                    if (bNoError)Audio->PlaySound(SOUND_BUTTON_SUCCESS);
                    break;
               case '3':
                    if (bNoError)Audio->PlaySound(SOUND_BUTTON_HOVER);
                    break;
               case '4':
                    if (bNoError)Audio->PlaySound(SOUND_GAME_OVER);
                    break;
               case '5':
                    if (bNoError)Audio->PlaySound(SOUND_GAME_VICTORY);
                    break;
               case '6':
                    if (bNoError)Audio->PlaySound(-4);
                    break;
               case '7':
                    if (bNoError)Audio->PlaySound(7);
                    break;
               case '8':
                    if (bNoError)Audio->StopMusic();
                    break;
        }
    }
    
    if (bNoError)delete Audio;
    return EXIT_SUCCESS;
}
