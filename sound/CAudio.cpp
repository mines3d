/* 
 * File:   CAudio.cpp
 * Author: Jindrich Kovar
 */
#include "CAudio.h"

#define BUFSIZE     100

#define PRINT_LOADING_ERROR(FILENAME) cerr << "Error: loading file " << FILENAME << " failed!" << endl;

CAudio::CAudio()
{   
    // String for exceptions
    char str[BUFSIZE];
    
    // Position of the listener.
    ListenerPos[0]=0.0; //{ 0.0, 0.0, 0.0 }
    ListenerPos[1]=0.0;
    ListenerPos[2]=0.0;
    
    // Velocity of the listener.
    ListenerVel[0]=0.0; //{ 0.0, 0.0, 0.0 }
    ListenerVel[1]=0.0;
    ListenerVel[2]=0.0;   
    
    // Orientation of the listener. (first 3 elements are "at", second 3 are "up") 
    ListenerOri[0]=0.0;// { 0.0, 0.0, -1.0, 0.0, 1.0, 0.0 }
    ListenerOri[1]=0.0;
    ListenerOri[2]=-1.0;
    ListenerOri[3]=0.0;
    ListenerOri[4]=1.0;
    ListenerOri[5]=0.0;
    
    // Default value (nothing is playing)
    iActuallyPlayingMusic=MUSIC_NOTHING;
    iActuallyPlayingSound=SOUND_NOTHING;
    
    // Initialize OpenAL and clear the error bit
	alutInit(NULL, 0);
	alGetError();
	
	// Error check 
	int error;
	if((error= alGetError() ) != AL_NO_ERROR)
    {        
        snprintf(str, BUFSIZE, "Cannot initialize OpenAl. (code %d)",error); str[BUFSIZE]='\0';
        throw CAudioException(str);
    }

	
	// Load wav data into buffers
	alGenBuffers(NUM_BUFFERS, Buffers);

    // Error check 
	if((error= alGetError() ) != AL_NO_ERROR)
    {
        snprintf(str, BUFSIZE, "Cannot generate buffers for sounds.(code %d)",error); str[BUFSIZE]='\0';
        throw CAudioException(str);
    }
	
    // Load music files
	if ( (Buffers[MUSIC_SLOW] = alutCreateBufferFromFile(MUSIC_SLOW_SOURCE))==0 )
		PRINT_LOADING_ERROR(MUSIC_SLOW_SOURCE)
    
	if ( (Buffers[MUSIC_FAST] = alutCreateBufferFromFile(MUSIC_FAST_SOURCE))==0 )
		PRINT_LOADING_ERROR(MUSIC_FAST_SOURCE)
	
	if ( (Buffers[SOUND_BUTTON_SUCCESS] = alutCreateBufferFromFile(SOUND_BUTTON_SUCCESS_SOURCE))==0 )
		PRINT_LOADING_ERROR(SOUND_BUTTON_SUCCESS_SOURCE)
    
	if ( (Buffers[SOUND_BUTTON_HOVER] = alutCreateBufferFromFile(SOUND_BUTTON_HOVER_SOURCE))==0 )
		PRINT_LOADING_ERROR(SOUND_BUTTON_HOVER_SOURCE)
    
	if ( (Buffers[SOUND_GAME_OVER] = alutCreateBufferFromFile(SOUND_GAME_OVER_SOURCE))==0 )
		PRINT_LOADING_ERROR(SOUND_GAME_OVER_SOURCE)
    
	if ( (Buffers[SOUND_GAME_VICTORY] = alutCreateBufferFromFile(SOUND_GAME_VICTORY_SOURCE))==0 )
		PRINT_LOADING_ERROR(SOUND_GAME_VICTORY_SOURCE)
	
    // Error check 
	if((error= alGetError() ) != AL_NO_ERROR)
    {
        snprintf(str, BUFSIZE, "Error loading sound files.(code %d)",error); str[BUFSIZE]='\0';
        throw CAudioException(str);
    }
	
	// Bind buffers into audio sources.
	alGenSources(NUM_SOURCES, Sources);
    
    // Error check 
	if((error= alGetError() ) != AL_NO_ERROR)
    {
        snprintf(str, BUFSIZE, "Cannot generate sources for sounds.(code %d)",error); str[BUFSIZE]='\0';
        throw CAudioException(str);
    }
    
    for (int i=0;i<NUM_SOURCES;i++)
    {
    	alSourcei (Sources[i], AL_BUFFER,   Buffers[i]   );
    	alSourcef (Sources[i], AL_PITCH,    1.0f         );
    	alSourcef (Sources[i], AL_GAIN,     1.0f         );
    	alSourcefv(Sources[i], AL_POSITION, SourcesPos[i]);
    	alSourcefv(Sources[i], AL_VELOCITY, SourcesVel[i]);
    	// Only music files are in loop
    	if (i<NUM_MUSICS) alSourcei (Sources[i], AL_LOOPING,  AL_TRUE);
	       else alSourcei (Sources[i], AL_LOOPING,  AL_FALSE );
    }

    // Error check 
	if((error= alGetError() ) != AL_NO_ERROR)
    {
        snprintf(str, BUFSIZE, "Cannot assign values to sources.(code %d)",error); str[BUFSIZE]='\0';
        throw CAudioException(str);
    }
		
	alListenerfv(AL_POSITION,    ListenerPos);
	alListenerfv(AL_VELOCITY,    ListenerVel);
	alListenerfv(AL_ORIENTATION, ListenerOri);
	
	// Error check 
	if((error= alGetError() ) != AL_NO_ERROR)
    {
        snprintf(str, BUFSIZE, "Cannot assign values to listener.(code %d)",error); str[BUFSIZE]='\0';
        throw CAudioException(str);
    }
	
	bSoundEnabled = true;
}
CAudio::~CAudio()
{
	alDeleteBuffers(NUM_BUFFERS, Buffers);
	alDeleteSources(NUM_SOURCES, Sources);
	alutExit();
}
/*!
 Function plays music by ID. It stops
 actually playing music if any.
 @param[in] ID = number of music source to play
 */
void CAudio::PlayMusic(int ID)
{
	if(ID == MUSIC_NOTHING) return StopMusic();
    if ((ID>-1)&&(ID<NUM_MUSICS))
    {
        if (iActuallyPlayingMusic!=MUSIC_NOTHING) StopMusic();                 
        alSourcePlay(Sources[ID]);  
        iActuallyPlayingMusic=ID;
    }   
}
/*!
 Function stops actually playing music.
 */
void CAudio::StopMusic(void)
{
    if (iActuallyPlayingMusic!=MUSIC_NOTHING)
    {
        alSourceStop(Sources[iActuallyPlayingMusic]);
    }
    iActuallyPlayingMusic=MUSIC_NOTHING;
}
/*!
 Function plays sound by ID.
 @param[in] ID = number of sound source to play
 */
void CAudio::PlayEffect(int ID)
{
	if(!bSoundEnabled) return;
    if ((ID>=NUM_MUSICS)&&(ID<NUM_SOURCES))
    {
        alSourcePlay(Sources[ID]);
        iActuallyPlayingSound=ID;
    }      
}
/*!
 Function stops actually playing sound or attempts to stop last played.
 */
void CAudio::StopEffect(void)
{
    if ( iActuallyPlayingSound!=SOUND_NOTHING)
    {
        alSourceStop(Sources[iActuallyPlayingSound]);
    }
    iActuallyPlayingSound=SOUND_NOTHING;
}
