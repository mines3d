/* 
 * File:   OutOfBoundsException.cpp
 * Author: Petr Kubizňák
 */

#include "OutOfBoundsException.h"

OutOfBoundsException::OutOfBoundsException(string desc) : GeneralException(ERR_OUT_OF_BOUNDS, desc) {
}

OutOfBoundsException::~OutOfBoundsException() {
}

