/* 
 * File:   AccessForbiddenException.cpp
 * Author: Petr Kubizňák
 */

#include "AccessForbiddenException.h"

AccessForbiddenException::AccessForbiddenException(string desc) : GeneralException(ERR_ACC_FORBIDDEN, desc) {
}

AccessForbiddenException::~AccessForbiddenException() {
}

