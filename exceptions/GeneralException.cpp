/* 
 * File:   GeneralException.cpp
 * Author: Petr Kubiznak
 */

#include "GeneralException.h"

GeneralException::GeneralException(int code, string desc) {
  this->errCode = code;
  this->errText = desc;
}

GeneralException::~GeneralException() {
}

