/* 
 * File:   CAudioException.h
 * Author: Jindrich Kovar
 */

#ifndef _CAUDIOEXCEPTION_H
#define	_CAUDIOEXCEPTION_H

#include "GeneralException.h"
#include <iostream>
using namespace std;

class CAudioException : public GeneralException {
public:
    CAudioException(string desc = "");
    ~CAudioException();
private:

};

#endif

