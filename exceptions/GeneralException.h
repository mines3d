/* 
 * File:   GeneralException.h
 * Author: Petr Kubiznak
 */

#ifndef _GENERALEXCEPTION_H
#define	_GENERALEXCEPTION_H

#include <iostream>
using namespace std;

//standardni chybove kody
#define ERR_DEFAULT         0x00
#define ERR_OUT_OF_BOUNDS   0x01       //pristup mimo pole
#define ERR_ACC_FORBIDDEN   0x02       //nepovoleny pristup k datum
#define ERR_AUDIO           0x03

class GeneralException {
private:

public:
    int errCode;
    string errText;

    GeneralException(int code = ERR_DEFAULT, string desc = "");
    ~GeneralException();
};

#endif	/* _GENERALEXCEPTION_H */

