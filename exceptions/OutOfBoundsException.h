/* 
 * File:   OutOfBoundsException.h
 * Author: Petr Kubiznak
 */

#ifndef _OUTOFBOUNDSEXCEPTION_H
#define	_OUTOFBOUNDSEXCEPTION_H

#include "GeneralException.h"
#include <iostream>
using namespace std;

class OutOfBoundsException : public GeneralException {
public:
    OutOfBoundsException(string desc = "");
    ~OutOfBoundsException();
private:

};

#endif	/* _OUTOFBOUNDSEXCEPTION_H */

