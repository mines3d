/* 
 * File:   CAudioException.cpp
 * Author: Jindrich Kovar
 */

#include "CAudioException.h"

CAudioException::CAudioException(string desc) : GeneralException(ERR_AUDIO, desc) {
}

CAudioException::~CAudioException() {
}

