/* 
 * File:   AccessForbiddenException.h
 * Author: Petr Kubiznak
 */

#ifndef _ACCESSFORBIDDENEXCEPTION_H
#define	_ACCESSFORBIDDENEXCEPTION_H

#include "GeneralException.h"
#include <iostream>
using namespace std;

class AccessForbiddenException : public GeneralException {
public:
    AccessForbiddenException(string desc = "");
    ~AccessForbiddenException();
private:

};

#endif	/* _ACCESSFORBIDDENEXCEPTION_H */

