/* 
 * File:   	ScoresForm.h
 * Created:	15.1.2010
 * Author: 	Petr Kubizňák
 * Purpose:	
 */

#ifndef SCORESFORM_H_
#define SCORESFORM_H_

#include "ui_ScoresForm.h"
#include "ScoreRecord.h"
#include "../../core/board.h"
#include <list>
#include <QtGui/QDialog>
#include <QtGui/QStandardItemModel>

/* -------------------------------------------------------------------------- */

class ScoresForm : public QDialog {
	Q_OBJECT
	
protected:
	ScoreRecord scr;
	QStandardItemModel *rookieModel, *advancedModel, *suicideModel, *customModel,
		*rookieSqlModel, *advancedSqlModel, *suicideSqlModel, *customSqlModel;
	
	/* zadany list zaznamu prevede na nove alokovany model, jehoz adresu vraci */
	QStandardItemModel *createModel(std::list<ScoreRecord> &source) const;

public:
	Ui::ScoresForm ui;

	/* vytvori novy formular; parametry jsou pointery na seznamy nejlepsich hracu */
	ScoresForm(std::list<ScoreRecord> *rookieList, std::list<ScoreRecord> *advancedList,
			std::list<ScoreRecord> *suicideList, std::list<ScoreRecord> *customList,
			std::list<ScoreRecord> *rookieSqlList, std::list<ScoreRecord> *advancedSqlList,
			std::list<ScoreRecord> *suicideSqlList, std::list<ScoreRecord> *customSqlList);
	virtual	~ScoresForm();
	/** Refreshes model of given parameters.
	 * @param mode Model type (difficulty).
	 * @param scoreList Source of actual data.
	 * @param local If true, local data is refreshed, false to refresh remote data (locally!). */
	void replaceModel(Board::GameMode mode, std::list<ScoreRecord> *scoreList, bool local=true);
	
public Q_SLOTS:
	/* udalost vyberu obtiznosti v ComboBoxu - nastavi prislusny model dat */
	void difficultnessCbx_Activated(int itemIndex);
	/** "Checkbox state changed" event.
	 * @param state Actual state (Qt::CheckState). */
	void sqlChbkb_Changed(int state);
	/* zobrazi modalne tento dialog s modelem daneho indexu */
	virtual int exec(int displayIndex);

};

#endif /* SCORESFORM_H_ */
