/* 
 * File:   	qt_gui.h
 * Created:	6.1.2010
 * Author: 	Petr Kubizňák
 * Purpose:	
 */


#ifndef QT_GUI_H_
#define QT_GUI_H_

/* spusti hru v qt grafickem modu */
int qtMain(int argc, char *argv[]);

#endif /* QT_GUI_H_ */
