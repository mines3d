/* 
 * File:   	AboutForm.h
 * Created:	20.1.2010
 * Author: 	Petr Kubizňák
 * Purpose:	
 */

#ifndef ABOUTFORM_H_
#define ABOUTFORM_H_

#include "ui_AboutForm.h"
#include <QtGui/QDialog>

/* -------------------------------------------------------------------------- */

class AboutForm : public QDialog {
	Q_OBJECT
	
protected:

public:
	Ui::AboutForm ui;

	AboutForm(void);
	virtual	~AboutForm(void);
	
};


#endif /* ABOUTFORM_H_ */
