/* 
 * File:   	ScoreRecord.h
 * Created:	15.1.2010
 * Author: 	Petr Kubizňák
 * Purpose:	Struct representing elementary data of a score table.
 */


#ifndef SCORERECORD_H_
#define SCORERECORD_H_

struct ScoreRecord {
	char name[50];
	int time;
};

#endif /* SCORERECORD_H_ */
