/* 
 * File:   	qt_gui.cpp
 * Created:	6.1.2010
 * Author: 	Petr Kubizňák
 * Purpose:	
 */

#include <QApplication>
#include "MainForm.h"

/* -------------------------------------------------------------------------- */

/* spusti hru v qt grafickem modu */
int qtMain(int argc, char *argv[]) {
	QApplication app(argc, argv);
	MainForm mainFrm;	
	mainFrm.show();
	app.exec();
	return 0;
}
