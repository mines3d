/* 
 * File:   	SettingsForm.h
 * Created:	11.1.2010
 * Author: 	Petr Kubizňák
 * Purpose:	
 */

#ifndef SETTINGSFORM_H_
#define SETTINGSFORM_H_

#include "ui_SettingsForm.h"
#include <QtGui/QDialog>

enum EMusicType {MUSIC_NO, MUSIC_1, MUSIC_2};

class SettingsForm : public QDialog {
	Q_OBJECT
	
protected:
	EMusicType musicType;
	
	/* nastavi vsem 4 spinum stejnou hodnotu enabled, tu vraci */
	bool setSpinsEnabled(bool value);
	/* nastavi vsem 4 spinum zadane hodnoty */
	void setSpinsValues(int layers, int rows, int columns, int mines);
		
public:
	Ui::SettingsForm ui;

	SettingsForm();
	virtual	~SettingsForm();
	
	void setSoundEffectsOn(bool value);
	void setSoundMusicType(EMusicType type);
	
	bool getSoundEffectsOn(void);
	EMusicType getSoundMusicType(void);
	
public Q_SLOTS:
	//stisk tlacitek modu
	void rookieRadio_Clicked(void);
	void advancedRadio_Clicked(void);
	void suicideRadio_Clicked(void);
	void customRadio_Clicked(void);
	//tlacitka zvuku
	void soundEffectsBtn_Clicked(void);
	void soundMusicBtn_Clicked(void);

};

#endif /* SETTINGSFORM_H_ */
