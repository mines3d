/*
 * File:   	MainForm.cpp
 * Created:	7.1.2010
 * Author: 	Petr Kubizňák
 * Purpose:	
 */

/* -------------------------------------------------------------------------- */

#include "MainForm.h"
#include "../../asserts.h"
#include "../../exceptions/GeneralException.h"
#include <QtGui/QPushButton>
#include <QtGui/QLabel>
#include <QtGui/QMouseEvent>
#include <QtGui/QInputDialog>
#include <QtGui/QMessageBox>
#include <cstdio>
#include <iostream>
#include <fstream>
using namespace std;

/***************************** CLASS FieldButton ******************************/

FieldButton::FieldButton(MainForm *mainForm, int posX, int posY, int posZ) : QPushButton(mainForm) {
	this->mainForm = mainForm;
	
	//obecne nastaveni
	this->posX = posX;
	this->posY = posY;
	this->posZ = posZ;
	this->setText("  ");
	this->setCheckable(true);
	this->setFocusPolicy(Qt::NoFocus);
	this->setMinimumWidth(23);
	this->setMinimumHeight(23);

	//rozmery komponent
	QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	sizePolicy.setHorizontalStretch(0);
	sizePolicy.setVerticalStretch(0);
	sizePolicy.setHeightForWidth(true);
	this->setSizePolicy(sizePolicy);
	
	//fonty: normalni a zvyrazneny (pro oznacena tlacitka)
	fontNormal = new QFont(this->font());
	fontHighlight = new QFont(this->font());
	fontHighlight->setUnderline(true);
	fontHighlight->setBold(true);
}

/* -------------------------------------------------------------------------- */

FieldButton::~FieldButton(void) {
	delete fontNormal;
	delete fontHighlight;
}

/* -------------------------------------------------------------------------- */

/* stisk tlacitka */
void FieldButton::onClick(bool b) {
	this->setChecked(true);
	if(b) mainForm->uncover(posZ, posY, posX);
}

/* -------------------------------------------------------------------------- */

/* udalost najeti mysi nad tlacitko */
void FieldButton::enterEvent(QEvent *e) {
	assert(mainForm);
	mainForm->buttonHovered(posZ, posY, posX);
	
	QPushButton::enterEvent(e);
}

/* -------------------------------------------------------------------------- */

/* udalost odjeti mysi z tlacitka */
void FieldButton::leaveEvent(QEvent *e) {
	assert(mainForm);
	mainForm->buttonLeft(posZ, posY, posX);
	
	QPushButton::leaveEvent(e);
}

/* -------------------------------------------------------------------------- */

/* udalost stisku tlacitka */
void FieldButton::mousePressEvent(QMouseEvent *e) {
	if(e->button() == Qt::RightButton) {
		if(!this->isChecked()) mainForm->mark(posZ, posY, posX);
	}
	
	if(e->button() == Qt::MiddleButton  )
	{
        mainForm->locked = !mainForm->locked;
    }
	
	QPushButton::mousePressEvent(e);
}

/* -------------------------------------------------------------------------- */

/* zvyrazni tlacitko zmenou fontu */
void FieldButton::highlight(void) {
	this->setFont(*fontHighlight);
}

/* -------------------------------------------------------------------------- */

/* "odvyrazni" tlacitko zpet */
void FieldButton::unhighlight(void) {
	this->setFont(*fontNormal);
}


/*********************************/// CLASS ///********************************/

/**************************** Class MarksIndicator ****************************/

/* -------------------------------------------------------------------------- */

MarksIndicator::MarksIndicator(QWidget *parent, int total) : QLabel(parent) {
	this->total = total;
	this->marked = 0;
	fontNormal = new QFont(this->font());
	fontBold = new QFont(this->font());
	fontBold->setBold(true);
}

/* -------------------------------------------------------------------------- */

MarksIndicator::~MarksIndicator(void) {
	delete fontNormal;
	delete fontBold;
}

/* -------------------------------------------------------------------------- */

/* prekresli komponentu */
void MarksIndicator::repaint(void) {
	char str[20];
	sprintf(str, "Marks: %d / %d ", marked, total);
	this->setText(str);
	this->setFont(marked <= total ? *fontNormal : *fontBold);
}

/* -------------------------------------------------------------------------- */

/* nastavi celkovy pocet vlajecek, totez vraci */
int MarksIndicator::setTotal(int value) {
	total = value;
	repaint();
	return value;
}

/* -------------------------------------------------------------------------- */

/* nastavi oznaceny pocet vlajecek, totez vraci */
int MarksIndicator::setMarked(int value) {
	marked = value;
	repaint();
	return value;
}

/*********************************/// CLASS ///********************************/

/******************************* Class MainForm *******************************/

//makro pro otestovani existence tlacitka se zadanym umistenim
#define TEST_COORDS(l,r,c) { assert(l>=0 && l<this->layers); \
assert(r>=0 && r<this->rows); assert(c>=0 && c<this->cols); }

//nazev souboru s nastavenim
#define FILE_SETTINGS					".mines3d_settings"
#define FILE_SCORES_ROOKIE		".mines3d_scores_rookie"
#define FILE_SCORES_ADVANCED	".mines3d_scores_advanced"
#define FILE_SCORES_SUICIDE		".mines3d_scores_suicide"
#define FILE_SCORES_CUSTOM		".mines3d_scores_custom"

/* -------------------------------------------------------------------------- */

MainForm::MainForm(QWidget *parent) : QMainWindow(parent) {
	ui.setupUi(this);
	
	//zrusime zbytecne okraje
	ui.scrollArea->setContentsMargins(0,0,0,0);
	ui.scrollAreaWidgetContents->setContentsMargins(0,0,0,0);
	ui.horizontalLayout->setContentsMargins(0,0,0,0);
	ui.boardWidget->setContentsMargins(0,0,0,0);
	ui.verticalLayout_3->setContentsMargins(0,0,0,0);
	
	//status bar
	marksIndicator = new MarksIndicator(this);
	statusBar()->addPermanentWidget(marksIndicator);
	
	//napojeni signalu na sloty
	QObject::connect(ui.actionNew, SIGNAL(triggered()), this, SLOT(actionNew_Trigger()));
	QObject::connect(ui.actionSettings, SIGNAL(triggered()), this, SLOT(actionSettings_Trigger()));
	QObject::connect(ui.actionScores, SIGNAL(triggered()), this, SLOT(actionScores_Trigger()));
	QObject::connect(ui.actionAbout, SIGNAL(triggered()), this, SLOT(actionAbout_Trigger()));

	board = NULL;
	settingsFrm = NULL;
	scoresFrm = NULL;
	aboutFrm = NULL;
	
	tc = new Thread_controller();
	sql_c = new Sql_connector(); 
	locked = false;

	//nacteme nastaveni ze souboru, pri neuspechu pouzijeme vychozi hodnoty
	if(!loadSettings()) {
		layers = PRESETS[0][0];
		rows = PRESETS[0][1];
		cols = PRESETS[0][2];
		mines = PRESETS[0][3];
		soundMusic = MUSIC_SLOW;
		audioController.bSoundEnabled = true;
	}
	if(soundMusic < 0 || soundMusic >= NUM_MUSICS) soundMusic = MUSIC_NOTHING;
	//nacteme vysledky predchozich her
	loadScores();
	loadRemoteScores();
	name = "<unknown>";				//vychozi jmeno uzivatele (pri zapisu do vysledku)

	//vyrobime novou hru
	newGame();
}

/* -------------------------------------------------------------------------- */

MainForm::~MainForm() {
	cout << "MAINFORM DESTRUCTOR" << endl;
	
	tc->cancel_glut_thread();
	pthread_join(tc->pthread1, NULL);
	
	freeGui();
	saveSettings();
	saveScores();
	delete settingsFrm;
	delete scoresFrm;
	delete aboutFrm;
	delete marksIndicator;
	
	delete tc;
}

/* -------------------------------------------------------------------------- */

/* nacte nastaveni ze standardniho souboru, vraci uspech operace */
bool MainForm::loadSettings(void) {
	ifstream ifs(FILE_SETTINGS, ifstream::in);
	if(!ifs.good()) return false;
	ifs >> layers >> rows >> cols >> mines >> soundMusic >> audioController.bSoundEnabled;
	if(!ifs.good() || layers<=0 || rows<=0 || cols<=0 || mines<=0) {
		ifs.close();
		return false;
	}
	ifs.close();
	return true;
}

/* -------------------------------------------------------------------------- */

/* ulozi nastaveni do standardniho souboru, vraci uspech operace */
bool MainForm::saveSettings(void) {
	ofstream ofs(FILE_SETTINGS, ofstream::out);
	if(!ofs.good()) return false;
	ofs << layers << " " << rows << " " << cols << " " << mines << " " << 
			soundMusic << " " << audioController.bSoundEnabled << endl << endl;
	if(!ofs.good()) return false;
	ofs.close();
	return true;
}

/* -------------------------------------------------------------------------- */

/* nacte vsechna skore, vraci uspech operace */
bool MainForm::loadScores(void) {
	return loadScoresFile(scoreRookie, FILE_SCORES_ROOKIE) &
		loadScoresFile(scoreAdvanced, FILE_SCORES_ADVANCED) &
		loadScoresFile(scoreSuicide, FILE_SCORES_SUICIDE) &
		loadScoresFile(scoreCustom, FILE_SCORES_CUSTOM);
}

/* -------------------------------------------------------------------------- */

/* ulozi vsechna skore, vraci uspech operace */
bool MainForm::saveScores(void) {
	return saveScoresFile(scoreRookie, FILE_SCORES_ROOKIE) &
		saveScoresFile(scoreAdvanced, FILE_SCORES_ADVANCED) &
		saveScoresFile(scoreSuicide, FILE_SCORES_SUICIDE) &
		saveScoresFile(scoreCustom, FILE_SCORES_CUSTOM);
}

/* -------------------------------------------------------------------------- */

/* nacte skore ze zadaneho souboru do daneho listu, vraci uspech operace */
bool MainForm::loadScoresFile(list<ScoreRecord> &scoresList, const char *filename) {
	ifstream ifs(filename, ifstream::in);
	if(!ifs.good()) return false;
	//dokud je co cist, cteme soubor do struktury
	while(true) {
		ScoreRecord scoreItem;
		ifs >> scoreItem.time;
		ifs.ignore(1);					//ignorujeme mezeru
		ifs.getline(scoreItem.name, sizeof(scoreItem.name));
		if(!ifs.good()) {	
			ifs.close();
			return true;
		}
		scoresList.push_back(scoreItem);
	}
}

/* -------------------------------------------------------------------------- */

/* ulozi skore do zadaneho souboru z daneho listu, vraci uspech operace */
bool MainForm::saveScoresFile(list<ScoreRecord> &scoresList, const char *filename) {
	ofstream ofs(filename, ofstream::out);
	if(!ofs.good()) return false;
	//zapiseme strukturu do souboru
	for(list<ScoreRecord>::iterator it = scoresList.begin(); it != scoresList.end(); it++)
		ofs << (*it).time << " " << (*it).name << endl;
	ofs << endl;
	if(!ofs.good()) return false;
	ofs.close();
	return true;
}

/* -------------------------------------------------------------------------- */

bool MainForm::loadRemoteScores(void) {
	bool b = true;
	sql_c->connect();
	b &= sql_c->getTop10(Board::ModeRookie   ,  scoreRookieSql);
	b &= sql_c->getTop10(Board::ModeAdvanced ,  scoreAdvancedSql);
	b &= sql_c->getTop10(Board::ModeSuicide  ,  scoreSuicideSql);
	b &= sql_c->getTop10(Board::ModeCustom   ,  scoreCustomSql);
	sql_c->disconnect();
	return b;
}

/* -------------------------------------------------------------------------- */

/* vraci true, pokud "a" predchazi pred "b" (podle casu), jinak false */
bool MainForm::cmpScoreByTime(const ScoreRecord &a, const ScoreRecord &b) {
	return a.time < b.time;
}

/* -------------------------------------------------------------------------- */

/* vyrobi tlacitka a dalsi komponenty potrebne pro hru
 * jako meze cyklu pouziva hodnoty layers, rows, cols a mines! */
void MainForm::buildGui(void) {
	layerWidget = new QWidget * [layers];
	layerGrid = new QGridLayout * [layers];
	fieldBtn = new FieldButton *** [layers];
	for(int i=0; i<layers; i++) {
		layerWidget[i] = new QWidget(ui.scrollAreaWidgetContents);
		ui.verticalLayout_3->addWidget(layerWidget[i]);
		layerGrid[i] = new QGridLayout(layerWidget[i]);
		layerGrid[i]->setHorizontalSpacing(0);
		layerGrid[i]->setVerticalSpacing(0);
		
		fieldBtn[i] = new FieldButton ** [rows];
		for(int j=0; j<rows; j++) {
			fieldBtn[i][j] = new FieldButton * [cols];
			for(int k=0; k<cols; k++) {
				fieldBtn[i][j][k] = new FieldButton(this, k, j, i);
				layerGrid[i]->addWidget(fieldBtn[i][j][k], j, k);
				QObject::connect(fieldBtn[i][j][k], SIGNAL(clicked(bool)), fieldBtn[i][j][k], SLOT(onClick(bool)));
			}
		}
	}
	
	tc->update_data(rows,cols,layers,board);
	tc->run_thread();
}

/* -------------------------------------------------------------------------- */

/* uvolni prostredky alokovane metodou buildGui() 
 * jako meze cyklu pouziva rozmery v board! */
void MainForm::freeGui(void) {
	for(int i=0; i<board->getLayersCount(); i++) {
		for(int j=0; j<board->getRowsCount(); j++) {
			for(int k=0; k<board->getColsCount(); k++) delete fieldBtn[i][j][k];
			delete [] fieldBtn[i][j];
		}
		delete [] fieldBtn[i];
	}
	delete [] fieldBtn;
	
	for(int i=0; i<board->getLayersCount(); i++) {
		delete layerGrid[i];
		delete layerWidget[i];
	}
	delete [] layerGrid;
	delete [] layerWidget;
}

/* -------------------------------------------------------------------------- */

/* volano tridou FieldButton pri udalosti enterEvent, zvyrazni souvisejici tlacitka */
void MainForm::buttonHovered(int l, int r, int c) {
	TEST_COORDS(l,r,c)
	
	if(gameOver) return;
	
	tc->mutex_lock();
	if (!locked)
	{
        tc->selected[0] = l;
        tc->selected[1] = r;
        tc->selected[2] = c;
        if ( tc->ptf != 0 )tc->ptf();
    }
    tc->selected[3] = l;
    tc->selected[4] = r;
    tc->selected[5] = c;
    tc->mutex_unlock();
	
    for(int nl=l-1; nl<=l+1; nl++) {
    	if(nl<0 || nl>=this->layers) continue;
		for(int nr=r-1; nr<=r+1; nr++) {
			if(nr<0 || nr>=this->rows) continue;
			for(int nc=c-1; nc<=c+1; nc++) {
				if(nc<0 || nc>=this->cols) continue;
				fieldBtn[nl][nr][nc]->highlight();
			}
		}
	}
    audioController.PlayEffect(SOUND_BUTTON_HOVER);
}

/* -------------------------------------------------------------------------- */

/* volano tridou FieldButton pri udalosti leaveEvent, odzvyrazni souvisejici tlacitka */
void MainForm::buttonLeft(int l, int r, int c) {
	TEST_COORDS(l,r,c)
	
	if(gameOver) return;
	
  for(int nl=l-1; nl<=l+1; nl++) {
  	if(nl<0 || nl>=this->layers) continue;
		for(int nr=r-1; nr<=r+1; nr++) {
			if(nr<0 || nr>=this->rows) continue;
			for(int nc=c-1; nc<=c+1; nc++) {
				if(nc<0 || nc>=this->cols) continue;
				fieldBtn[nl][nr][nc]->unhighlight();
			}
		}
	}	
}

/* -------------------------------------------------------------------------- */

/* volano tridou FieldButton pri stisku tlacitka, odkryje souvisejici tlacitka */
int MainForm::uncover(int l, int r, int c) {
	TEST_COORDS(l,r,c)
	char numStr[10];
	
	startTime();			//pokud jeste nemerime cas, zacneme nyni

	//pole je oznacene jako zaminovane, nejde odkryt
	if(board->getField(l,r,c).hasMark()) {
		fieldBtn[l][r][c]->setChecked(false);
		audioController.PlayEffect (SOUND_NOTHING);
		return SOUND_NOTHING;
	}
	
	//odkryjeme pole
	if(board->uncover(l, r, c)) {							//VYBUCH!
		fieldBtn[l][r][c]->setText("!!");
		audioController.PlayEffect (SOUND_GAME_OVER);
		setGameOver(false);
		return SOUND_GAME_OVER;
	} else {
		int num = board->getField(l,r,c).getNeighboursCnt();
		if(num) {
			sprintf(numStr, "%d", num);
			fieldBtn[l][r][c]->setText(numStr);		//vypiseme pocet min v okoli
		} else {				//0 min -> musime zaktualizovat celou desku
			for(int i=0; i<this->layers; i++)
				for(int j=0; j<this->rows; j++)
					for(int k=0; k<this->cols; k++) {
						fieldBtn[i][j][k]->setChecked(!board->getField(i,j,k).isCovered());
						if(fieldBtn[i][j][k]->isChecked()) {
							num = board->getField(i,j,k).getNeighboursCnt();
							sprintf(numStr, "%d", num);
							if(num) fieldBtn[i][j][k]->setText(numStr);
						}
					}
		}
		if(board->isCleared()) {
			audioController.PlayEffect (SOUND_GAME_VICTORY);
			setGameOver(true);
			return SOUND_GAME_VICTORY;
		}
	}
	audioController.PlayEffect (SOUND_BUTTON_SUCCESS);
	return SOUND_BUTTON_SUCCESS;
}

/* -------------------------------------------------------------------------- */

/* volano tridou FieldButton pri stisku praveho tlacitka, (od)znaci dane policko */
void MainForm::mark(int l, int r, int c) {
	TEST_COORDS(l,r,c)

	startTime();			//pokud jeste nemerime cas, zacneme nyni
	
	const Field &field = board->getField(l, r, c);
	if(!field.isCovered()) return;
	board->setFieldMark(l,r,c, !field.hasMark());
	fieldBtn[l][r][c]->setText(field.hasMark() ? "M" : "  ");			//znacka
	
	marksIndicator->setMarked(board->getMarkedCount());
}

/* -------------------------------------------------------------------------- */

/* nastavi pocatecni cas hry */
void MainForm::startTime(void) {
	if(tStart==-1) tStart = time(NULL);				//pocatek mereni casu
}

/* -------------------------------------------------------------------------- */

/* vytvori novou hru s aktualnim nastavenim */
void MainForm::newGame(void) {
	if(board) 
    {
        tc->mutex_lock();
        freeGui();		//uvolni stare gui (jeste podle stareho nastaveni)
        delete board; board = NULL;
        tc->b = 0;
        locked = false;
        tc->mutex_unlock();
    }
	try {
		board = new Board(rows, cols, layers, mines);
	} catch(GeneralException e) {
		char text[200];
		snprintf(text, 199, "An error occured:\n%s\nUnable to start game.", e.errText.data());
		QMessageBox::warning(this, "Mines3D", text, QMessageBox::Ok);
		return;
	}
	ui.scrollAreaWidgetContents->setEnabled(true);
	gameOver = false;
	marksIndicator->setTotal(mines);
	marksIndicator->setMarked(0);
	buildGui();
	tStart = -1;			//indikuje, ze jeste nezacalo mereni (zacne prvnim kliknutim)
	audioController.PlayMusic(soundMusic);
}

/* -------------------------------------------------------------------------- */

/* ukonci hru -> znemozni stisk tlacitek atp.;
 * atr success je true, pokud uzivatel vyhral, jinak false */
void MainForm::setGameOver(bool success) {
	audioController.StopMusic();
	
	gameOver = true;
	tStop = time(NULL);
	ui.scrollAreaWidgetContents->setEnabled(false);
	
	//odvyraznime vsechna tlacitka a vykreslime polohy min
	for(int i=0; i<this->layers; i++) {
		for(int j=0; j<this->rows; j++)
			for(int k=0; k<this->cols; k++) {
				fieldBtn[i][j][k]->unhighlight();
				if(board->getField(i,j,k).hasMine())
					fieldBtn[i][j][k]->setText(success ? "M" : "!!");
			}
	}
	//vyhodnoceni vysledku
	if(!success) {
		statusBar()->showMessage("Oooops, you stepped on a mine which exploxed!", 10000);
	} else {
		char str[100];
		const int tTotal = tStop-tStart;
		sprintf(str, "Very well, mister! You cleared the board in %d:%02d.", tTotal/60, tTotal%60);
		statusBar()->showMessage(str, 10000);
		processTopTen(tTotal);				//zapis do tabulky (pokud je cas v top10)
	}
}

/* -------------------------------------------------------------------------- */

/* pokud je zadany cas v top10, umozni zapis do tabulky a vraci true, jinak false */
bool MainForm::processTopTen(int time) {
	bool ok;
	
	list<ScoreRecord> *l, *ld;
	switch(board->getMode()) {
	case Board::ModeRookie:     l=&scoreRookie;   ld=&scoreRookieSql;   break;
	case Board::ModeAdvanced:   l=&scoreAdvanced; ld=&scoreAdvancedSql; break;
	case Board::ModeSuicide:    l=&scoreSuicide;  ld=&scoreSuicideSql;  break;
	case Board::ModeCustom:     l=&scoreCustom;   ld=&scoreCustomSql;   break;
	default:                    return false;
	}
	
	l->sort(cmpScoreByTime);															//seradime vzestupne podle casu
	//je-li v seznamu jeste misto nebo nejhorsi cas je horsi, nez soucasny, jedna se o top10 -> pridame zaznam
	if(l->size() < 10 || l->back().time > time) {
		
		//vyzveme uzivatele k zadani jmena
		QString text = QInputDialog::getText(this, "Top 10!", "You have made the top 10!\nPlease type in your name:",
				QLineEdit::Normal, name, &ok);
		//pokud bylo stisknuto OK
		if(ok) name = text;		//zapamatujem si vlozene jmeno
		else return false;											//uzivatel si nepreje vlozit zaznam
		
		sql_c->connect();                                     // nahrani dat do databaze		
		sql_c->insertScore(name.toUtf8().data(), time, board->getMode());
		sql_c->getTop10(board->getMode(),*ld);
		sql_c->disconnect();
		
		ScoreRecord scoreItem;
		snprintf(scoreItem.name, 40, "%s", name.toUtf8().data());
		scoreItem.time = time;
		if(l->size() >= 10) l->pop_back();							//neni jiz misto -> vymazeme nejhorsi cas
		l->push_back(scoreItem);												//vlozime zaznam
		if(scoresFrm) scoresFrm->replaceModel(board->getMode(), l);		//zaktualizujeme model dat ve formulari vysledku
		if(scoresFrm) scoresFrm->replaceModel(board->getMode(), ld, false);
		actionScores_Trigger();													//zobrazime vysledky
		return true;
	}
	return false;								//v seznamu neni misto (cas nebyl dostatecne rychly)
}

/* -------------------------------------------------------------------------- */

/* stisk tlacitka "New" v menu */
void MainForm::actionNew_Trigger(void) {
	newGame();
}

/* -------------------------------------------------------------------------- */

/* stisk tlacitka "Settings..." v menu */
void MainForm::actionSettings_Trigger(void) {
	if(!settingsFrm) settingsFrm = new SettingsForm();
	
	//zobrazime aktualni nastaveni
	int mode = board ? board->getMode() : Board::ModeRookie;
	switch(mode) {
	case Board::ModeRookie:
		settingsFrm->ui.rookieRadio->setChecked(true);
		settingsFrm->rookieRadio_Clicked();
		break;
	case Board::ModeAdvanced:
		settingsFrm->ui.advancedRadio->setChecked(true);
		settingsFrm->advancedRadio_Clicked();
		break;
	case Board::ModeSuicide:
		settingsFrm->ui.suicideRadio->setChecked(true);
		settingsFrm->suicideRadio_Clicked();
		break;
	case Board::ModeCustom:
		settingsFrm->ui.customRadio->setChecked(true);
		settingsFrm->ui.layersSpinBox->setValue(this->layers);
		settingsFrm->ui.rowsSpinBox->setValue(this->rows);
		settingsFrm->ui.columnsSpinBox->setValue(this->cols);
		settingsFrm->ui.minesSpinBox->setValue(this->mines);
		settingsFrm->customRadio_Clicked();
		break;
	}
	
	settingsFrm->setSoundEffectsOn(audioController.bSoundEnabled);
	EMusicType mType;
	switch(soundMusic) {
	case MUSIC_SLOW: mType = MUSIC_1;  break;
	case MUSIC_FAST: mType = MUSIC_2;  break;
	default:         mType = MUSIC_NO; break;
	}
	settingsFrm->setSoundMusicType(mType);
	
	//zobrazime dialog a vyhodnotime odpoved
	if(settingsFrm->exec() == QDialog::Accepted) {
		layers = settingsFrm->ui.layersSpinBox->value();
		rows = settingsFrm->ui.rowsSpinBox->value();
		cols = settingsFrm->ui.columnsSpinBox->value();
		mines = settingsFrm->ui.minesSpinBox->value();
		
		audioController.bSoundEnabled = settingsFrm->getSoundEffectsOn();
		switch(settingsFrm->getSoundMusicType()) {
		case MUSIC_1: soundMusic = MUSIC_SLOW;    break;
		case MUSIC_2: soundMusic = MUSIC_FAST;    break;
		default:      soundMusic = MUSIC_NOTHING; break;
		}
		audioController.PlayMusic(soundMusic);
		
		if((board->getLayersCount()!=layers) || (board->getRowsCount()!=rows) ||
				(board->getColsCount()!=cols) || (board->getMinesCount()!=mines)) newGame();
	}
}

/* -------------------------------------------------------------------------- */

/* stisk tlacitka "Hall of Fame" v menu */
void MainForm::actionScores_Trigger(void) {
	if(!scoresFrm) scoresFrm = new ScoresForm(
			&scoreRookie, &scoreAdvanced, &scoreSuicide, &scoreCustom,
			&scoreRookieSql, &scoreAdvancedSql, &scoreSuicideSql, &scoreCustomSql);
	scoresFrm->exec(board->getMode());
}

/* -------------------------------------------------------------------------- */

void MainForm::actionAbout_Trigger(void) {
	if(!aboutFrm) aboutFrm = new AboutForm();
	aboutFrm->exec();
}

/* -------------------------------------------------------------------------- */
