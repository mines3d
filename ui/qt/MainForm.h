/* 
 * File:   	MainForm.h
 * Created:	7.1.2010
 * Author: 	Petr Kubizňák
 * Purpose:	
 */

#ifndef MAINFORM_H_
#define MAINFORM_H_

#include "ui_MainForm.h"
#include "SettingsForm.h"
#include "ScoresForm.h"
#include "ScoreRecord.h"
#include "AboutForm.h"
#include "../../core/board.h"
#include "../../threads/thread_controller.h"
#include "../../sqlconn/sql_connector.h"
#include "../../sound/CAudio.h"
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <ctime>
#include <list>

class MainForm;

/***************************** CLASS FieldButton ******************************/

/* extends QPushButton, adds information about the buttons position */
class FieldButton : public QPushButton {
	Q_OBJECT
	
protected:
	MainForm *mainForm;
	int posX, posY, posZ;
	QFont *fontHighlight, *fontNormal;
	
	/* udalost najeti mysi */
	void enterEvent(QEvent *e);
	/* udalost odjeti mysi */
	void leaveEvent(QEvent *e);
	/* udalost stisku tlacitka */
	void mousePressEvent(QMouseEvent *e);

public:
	FieldButton(MainForm *mainForm, int posX = 0, int posY = 0, int posZ = 0);
	virtual ~FieldButton(void);
	
	/* zvyrazni tlacitko zmenou fontu */
	void highlight(void);
	/* "odvyrazni" tlacitko zpet */
	void unhighlight(void);
	
public Q_SLOTS:
	void onClick(bool b);

};

/**************************** Class MarksIndicator ****************************/

/* Displays number of marks and total count of mines on the status bar */
class MarksIndicator : public QLabel {
protected:
	int marked, total;
	QFont *fontNormal, *fontBold;
	
	/* prekresli komponentu */
	void repaint(void);
	
public:
	MarksIndicator(QWidget *parent, int total = 0);
	virtual ~MarksIndicator(void);
	
	/* nastavi celkovy pocet vlajecek, totez vraci */
	int setTotal(int value);
	/* nastavi oznaceny pocet vlajecek, totez vraci */
	int setMarked(int value);
	
};

/******************************* CLASS MainForm *******************************/

class MainForm : public QMainWindow {
	Q_OBJECT
	
protected:
	Ui::MainForm ui;
	Board *board;
	FieldButton ****fieldBtn;
	QWidget **layerWidget;
	QGridLayout **layerGrid;
	SettingsForm *settingsFrm;
	ScoresForm *scoresFrm;
	MarksIndicator *marksIndicator;
	AboutForm *aboutFrm;
	
	int layers, rows, cols, mines;					//pro vetsi rychlost a pohodli duplikuje data z board
	int soundMusic;                                                             //!< Type of selected music to play.
	bool gameOver;
	QString name;							//jmeno uzivatele
	time_t tStart, tStop;
	list<ScoreRecord> scoreRookie, scoreAdvanced, scoreSuicide, scoreCustom,
			scoreRookieSql, scoreAdvancedSql, scoreSuicideSql, scoreCustomSql;
	
	/* vyrobi tlacitka a dalsi komponenty potrebne pro hru */
	void buildGui(void);
	/* uvolni prostredky alokovane metodou buildGui() */
	void freeGui(void);
	/* vytvori novou hru s aktualnim nastavenim */
	void newGame(void);
	/* ukonci hru -> znemozni stisk tlacitek atp.;
	 * atr success je true, pokud uzivatel vyhral, jinak false */
	void setGameOver(bool success);
	/* nastavi pocatecni cas hry */
	void startTime(void);
	/* nacte nastaveni ze standardniho souboru, vraci uspech operace */
	bool loadSettings(void);
	/* ulozi nastaveni do standardniho souboru, vraci uspech operace */
	bool saveSettings(void);
	/* nacte vsechna skore, vraci uspech operace */
	bool loadScores(void);
	/* ulozi vsechna skore, vraci uspech operace */
	bool saveScores(void);
	/* nacte skore ze zadaneho souboru do daneho listu, vraci uspech operace */
	bool loadScoresFile(list<ScoreRecord> &scoresList, const char *filename);
	/* ulozi skore do zadaneho souboru z daneho listu, vraci uspech operace */
	bool saveScoresFile(list<ScoreRecord> &scoresList, const char *filename);
	/** Loads scores from remote database.
	 * @return True on success, false on fail. */
	bool loadRemoteScores(void);
	/* vraci true, pokud "a" predchazi pred "b" (podle casu), jinak false */
	static bool cmpScoreByTime(const ScoreRecord &a, const ScoreRecord &b);
	/* pokud je zadany cas v top10, umozni zapis do tabulky a vraci true, jinak false */
	bool processTopTen(int time);

public:
    
	CAudio audioController;
	
    Thread_controller * tc;
    Sql_connector * sql_c;
    bool locked;
    
	MainForm(QWidget *parent = 0);
	virtual ~MainForm();
	
	/* volano tridou FieldButton pri udalosti enterEvent, zvyrazni souvisejici tlacitka */
	void buttonHovered(int l, int r, int c);
	/* volano tridou FieldButton pri udalosti leaveEvent, odzvyrazni souvisejici tlacitka */
	void buttonLeft(int l, int r, int c);
	/** Uncoveres button on given position (and related buttons). Is called by FieldButton on click.
	 * @return SOUND_BUTTON_SUCCESS if no mine is hidden here, SOUND_GAME_OVER if mine exploded,
	 *  SOUND_GAME_VICTORY if no mine is hidden and the last field is uncovered,
	 *  SOUND_NOTHING if the field is marked as mined (so it cannot be uncovered). */
	int uncover(int l, int r, int c);
	/* volano tridou FieldButton pri stisku praveho tlacitka, (od)znaci dane policko */
	void mark(int l, int r, int c);
	
public Q_SLOTS:
	void actionSettings_Trigger(void);
	void actionNew_Trigger(void);
	void actionScores_Trigger(void);
	void actionAbout_Trigger(void);

};

#endif /* MAINFORM_H_ */
