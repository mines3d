/* 
 * File:   	ScoresForm.cpp
 * Created:	15.1.2010
 * Author: 	Petr Kubizňák
 * Purpose:	
 */

/* -------------------------------------------------------------------------- */

#include "ScoresForm.h"
#include "../../asserts.h"
#include <cstdio>

/* -------------------------------------------------------------------------- */

/* vytvori novy formular; parametry jsou pointery na seznamy nejlepsich hracu */
ScoresForm::ScoresForm(std::list<ScoreRecord> *rookieList, std::list<ScoreRecord> *advancedList,
		std::list<ScoreRecord> *suicideList, std::list<ScoreRecord> *customList,
		std::list<ScoreRecord> *rookieSqlList, std::list<ScoreRecord> *advancedSqlList,
		std::list<ScoreRecord> *suicideSqlList, std::list<ScoreRecord> *customSqlList) {
	ui.setupUi(this);
	
	//polozky comboBoxu
	ui.difficultnessCbx->addItem("Rookie");
	ui.difficultnessCbx->addItem("Advanced");
	ui.difficultnessCbx->addItem("Suicide");
	ui.difficultnessCbx->addItem("Custom");

	//vytvorime modely z listu
	rookieModel   = createModel(*rookieList);
	advancedModel = createModel(*advancedList);
	suicideModel  = createModel(*suicideList);
	customModel   = createModel(*customList);
	rookieSqlModel   = createModel(*rookieSqlList);
	advancedSqlModel = createModel(*advancedSqlList);
	suicideSqlModel  = createModel(*suicideSqlList);
	customSqlModel   = createModel(*customSqlList);
	
	ui.scoresView->setSortingEnabled(true);

	QObject::connect(ui.difficultnessCbx, SIGNAL(activated(int)), this, SLOT(difficultnessCbx_Activated(int)));
	QObject::connect(ui.sqlChkb, SIGNAL(stateChanged(int)), this, SLOT(sqlChbkb_Changed(int)));
}

/* -------------------------------------------------------------------------- */

ScoresForm::~ScoresForm() {
	delete rookieModel;
	delete advancedModel;
	delete suicideModel;
	delete customModel;
}

/* -------------------------------------------------------------------------- */

/* zadany list zaznamu prevede na nove alokovany model, jehoz adresu vraci */
QStandardItemModel *ScoresForm::createModel(std::list<ScoreRecord> &source) const {
	char time[50];
	char order[20];
	
	QStandardItemModel *model = new QStandardItemModel(0, 3, ui.scoresView);
	model->setHeaderData(0, Qt::Horizontal, "Name");
	model->setHeaderData(1, Qt::Horizontal, "Time");
	model->setHeaderData(2, Qt::Horizontal, "#");
	for(std::list<ScoreRecord>::iterator it = source.begin(); it != source.end(); it++) {
		snprintf(time, 49, "%d:%02d", (*it).time / 60, (*it).time % 60);
		snprintf(order, 19, "%010d", (*it).time);
		
		model->insertRow(0);
		model->setData(model->index(0, 0), QString((*it).name), Qt::DisplayRole);
		model->setData(model->index(0, 1), time, Qt::DisplayRole);
		model->setData(model->index(0, 2), order, Qt::DisplayRole);
	}
	return model;
}

/* -------------------------------------------------------------------------- */

/* provede aktualizaci (nahradu) modelu daneho modem daty v listu */
void ScoresForm::replaceModel(Board::GameMode mode, std::list<ScoreRecord> *scoreList, bool local) {
	QStandardItemModel **model;
	switch(mode) {
	case Board::ModeRookie:
		model = (local ? &rookieModel : &rookieSqlModel);
		break;
	
	case Board::ModeAdvanced:
		model = (local ? &advancedModel : &advancedSqlModel);
		break;
	
	case Board::ModeSuicide:
		model = (local ? &suicideModel : &suicideSqlModel);
		break;

	case Board::ModeCustom:
		model = (local ? &customModel : &customSqlModel);
		break;
	}
	
	delete (*model);
	*model = createModel(*scoreList);
}

/* -------------------------------------------------------------------------- */

/* zobrazi modalne tento dialog s modelem daneho indexu */
int ScoresForm::exec(int displayIndex) {
	assert(displayIndex >=0 && displayIndex <=3);

	ui.difficultnessCbx->setCurrentIndex(displayIndex);
	difficultnessCbx_Activated(displayIndex);
	ui.scoresView->setColumnWidth(0, 200);
	ui.scoresView->setColumnWidth(1, 100);
	
	return QDialog::exec();	
}

/* -------------------------------------------------------------------------- */

/* udalost vyberu obtiznosti v ComboBoxu - nastavi prislusny model dat */
void ScoresForm::difficultnessCbx_Activated(int itemIndex) {
	QStandardItemModel *model;
	bool bRemote = ui.sqlChkb->checkState()==Qt::Checked;
	
	switch(itemIndex) {
	case 0:   model = (bRemote ? rookieSqlModel   : rookieModel);   break;
	case 1:   model = (bRemote ? advancedSqlModel : advancedModel); break;
	case 2:   model = (bRemote ? suicideSqlModel  : suicideModel);  break;
	default:  model = (bRemote ? customSqlModel   : customModel);   break;
	}
	ui.scoresView->setModel(model);
	ui.scoresView->setColumnHidden(2,true);
	ui.scoresView->sortByColumn(2, Qt::AscendingOrder);		//seradime podle casu
}

/* -------------------------------------------------------------------------- */

void ScoresForm::sqlChbkb_Changed(int state) {
	difficultnessCbx_Activated(ui.difficultnessCbx->currentIndex());
}
