/* 
 * File:   	SettingsForm.cpp
 * Created:	11.1.2010
 * Author: 	Petr Kubizňák
 * Purpose:	
 */

/* -------------------------------------------------------------------------- */

#include "SettingsForm.h"
#include "../../core/board.h"

/* -------------------------------------------------------------------------- */

SettingsForm::SettingsForm() {
	ui.setupUi(this);
	
	QObject::connect(ui.rookieRadio, SIGNAL(clicked()), this, SLOT(rookieRadio_Clicked()));
	QObject::connect(ui.advancedRadio, SIGNAL(clicked()), this, SLOT(advancedRadio_Clicked()));
	QObject::connect(ui.suicideRadio, SIGNAL(clicked()), this, SLOT(suicideRadio_Clicked()));
	QObject::connect(ui.customRadio, SIGNAL(clicked()), this, SLOT(customRadio_Clicked()));
	
	QObject::connect(ui.soundEffectsBtn, SIGNAL(clicked()), this, SLOT(soundEffectsBtn_Clicked()));
	QObject::connect(ui.soundMusicBtn, SIGNAL(clicked()), this, SLOT(soundMusicBtn_Clicked()));
}

/* -------------------------------------------------------------------------- */

SettingsForm::~SettingsForm() {
	
}

/* -------------------------------------------------------------------------- */

/* nastavi vsem 4 spinum stejnou hodnotu enabled, tu vraci */
bool SettingsForm::setSpinsEnabled(bool value) {
	ui.layersSpinBox->setEnabled(value);
	ui.rowsSpinBox->setEnabled(value);
	ui.columnsSpinBox->setEnabled(value);
	ui.minesSpinBox->setEnabled(value);
	return value;
}

/* -------------------------------------------------------------------------- */

/* nastavi vsem 4 spinum zadane hodnoty */
void SettingsForm::setSpinsValues(int layers, int rows, int columns, int mines) {
	ui.layersSpinBox->setValue(layers);
	ui.rowsSpinBox->setValue(rows);
	ui.columnsSpinBox->setValue(columns);
	ui.minesSpinBox->setValue(mines);
}

/* -------------------------------------------------------------------------- */

void SettingsForm::setSoundEffectsOn(bool value) {
	ui.soundEffectsBtn->setText(value ? "Effects On" : "Effects Off");
	ui.soundEffectsBtn->setChecked(value);
}

/* -------------------------------------------------------------------------- */

void SettingsForm::setSoundMusicType(EMusicType type) {
	musicType = type;
	
	switch(musicType) {
	case MUSIC_NO:
		ui.soundMusicBtn->setText("Music Off");
		ui.soundMusicBtn->setChecked(false);
		break;
	case MUSIC_1:
		ui.soundMusicBtn->setText("Music 1");
		ui.soundMusicBtn->setChecked(true);
		break;
	case MUSIC_2:
		ui.soundMusicBtn->setText("Music 2");
		ui.soundMusicBtn->setChecked(true);
		break;
	}
}

/* -------------------------------------------------------------------------- */

bool SettingsForm::getSoundEffectsOn(void) {
	return ui.soundEffectsBtn->isChecked();
}

/* -------------------------------------------------------------------------- */

EMusicType SettingsForm::getSoundMusicType(void) {
	return musicType;
}

/* -------------------------------------------------------------------------- */

void SettingsForm::rookieRadio_Clicked(void) {
	setSpinsEnabled(false);
	setSpinsValues(PRESETS[0][0], PRESETS[0][1], PRESETS[0][2], PRESETS[0][3]);
}

/* -------------------------------------------------------------------------- */

void SettingsForm::advancedRadio_Clicked(void) {
	setSpinsEnabled(false);
	setSpinsValues(PRESETS[1][0], PRESETS[1][1], PRESETS[1][2], PRESETS[1][3]);
}

/* -------------------------------------------------------------------------- */

void SettingsForm::suicideRadio_Clicked(void) {
	setSpinsEnabled(false);
	setSpinsValues(PRESETS[2][0], PRESETS[2][1], PRESETS[2][2], PRESETS[2][3]);
}

/* -------------------------------------------------------------------------- */

void SettingsForm::customRadio_Clicked(void) {
	setSpinsEnabled(true);
}

/* -------------------------------------------------------------------------- */

void SettingsForm::soundEffectsBtn_Clicked(void) {
	setSoundEffectsOn(ui.soundEffectsBtn->isChecked());
}

/* -------------------------------------------------------------------------- */

void SettingsForm::soundMusicBtn_Clicked(void) {
	switch(musicType) {
	case MUSIC_NO: setSoundMusicType(MUSIC_1);  break;
	case MUSIC_1:  setSoundMusicType(MUSIC_2);  break;
	case MUSIC_2:  setSoundMusicType(MUSIC_NO); break;
	}
}

/* -------------------------------------------------------------------------- */
