/* 
 * File:   	console.cpp
 * Created:	6.1.2010
 * Author: 	Petr Kubizňák
 * Purpose:
 */

/* -------------------------------------------------------------------------- */

#include "console.h"
#include "../../core/board.h"
#include "../../asserts.h"
#include "../../exceptions/AccessForbiddenException.h"
#include <iostream>
#include <iomanip>
#include <climits>
using namespace std;

/* -------------------------------------------------------------------------- */

#define READ_AGAIN(msg1, msg2) { cin.clear(); cin.ignore(INT_MAX, '\n'); cout << (msg1) << (msg2) << endl; continue; }

/* -------------------------------------------------------------------------- */

//casto vypisovane retezce
const char *INVALID_INPUT = "Invalid input! ";
const char *SYNTAX = "Correct syntax corresponds to \"C n_L n_R n_C\", where \'C\' represents your command (possible values: 'U'=uncover, "
		"'M'=(un)mask), \'n_L\' represents an ordinal number of layer, \'n_R\' a row and \'n_C\' a column. "
		"For example expression \"U 2 6 3\" means you want to uncover a layer number two ( = third layer; indexed from zero), row six and column three.";

/* -------------------------------------------------------------------------- */

/* vytiskne pole nastaveni do vystupniho streamu; velikost pole MUSI byt (aspon) 4! */
ostream & operator << (ostream &os, const int settingsArray[]) {
	os << "< " << settingsArray[0] << " x " << settingsArray[1] << " x " <<
		settingsArray[2] << " , " << setw(4) << settingsArray[3] << " mines >";
	return os;
}

/* -------------------------------------------------------------------------- */

/* umozni uzivateli zvolit rozmery desky a pocet min; hodnoty predava odkazem */
void readBoardSettings(int *layers, int *rows, int *cols, int *mines) {
	assert(layers);	assert(rows);	assert(cols);	assert(mines);
	int num;

	cout << "Select settings of the game:" << endl;
	cout << "\t[0]: Rookie\t" << PRESETS[0] << endl;
	cout << "\t[1]: Advanced\t" << PRESETS[1] << endl;
	cout << "\t[2]: Suicide\t" << PRESETS[2] << endl;

	while(true) {
		cout << "Number: ";
		cin >> num;
		if(cin.good() && num >= 0 && num < 3) break;
		READ_AGAIN(INVALID_INPUT, "");
	}
	*layers = PRESETS[num][0];
	*rows 	= PRESETS[num][1];
	*cols 	= PRESETS[num][2];
	*mines 	= PRESETS[num][3];
}

/* -------------------------------------------------------------------------- */

/* precte ze vstupu pozadovane pole;
 * vraci true, pokud se ma odkryt, false, pokud se ma oznacit */
bool readChoice(int *layer, int *row, int *col, const Board *b) {
	while(true) {
		char command;
		cout << "Choice: ";

		cin >> command;																										//prikaz
		if(!cin.good()) READ_AGAIN(INVALID_INPUT, SYNTAX);
		if(toupper(command) != 'U' && toupper(command) != 'M')
			READ_AGAIN(INVALID_INPUT, "Possible commands are only 'U' and 'M'.");

		cin >> (*layer) >> (*row) >> (*col);															//souradnice
		if(!cin.good()) READ_AGAIN(INVALID_INPUT, SYNTAX);
		if((*layer >= 0) && (*layer < b->getLayersCount()) && (*row >= 0) &&
				(*row < b->getRowsCount()) && (*col >=0) && (*col < b->getColsCount()))
			return (toupper(command) == 'U');

		READ_AGAIN(INVALID_INPUT, "Some index is out of bounds.");
	}
}

/* -------------------------------------------------------------------------- */

/* spusti hru v konzolovem modu */
int consoleMain(void) {
	//uvodni instrukce
	cout << " ***** MINES3D *****" << endl << endl;
	cout << "Welcome to Mines3D - extended minesweeper game!" << endl << endl;
	cout << "The rules are easy - uncover every field on the board except those where mines are hidden." << endl <<
		"The classic game is extended by a third dimension, so there is a possibility of up to 26 mines around one field." << endl << endl;
	cout << "Just select the settings of the board (size and count of mines) and start playing." << endl;
	cout << "You only need to periodically type in which field to uncover. Use a specified syntax: " << SYNTAX << endl;

	//vyber nastaveni
	cout << endl << " ***** SETTINGS *****" << endl << endl;
	int l, r, c, m;
	readBoardSettings(&l, &r, &c, &m);
	Board board(r, c, l, m);

	cout << endl << " ***** GAME *****" << endl << endl;
	cout << board;

	// smycka hry
	while(true) {
		bool uncover = readChoice(&l, &r, &c, &board);
		if(uncover) {																//prikaz = ODKRYT
			if(!(board(l,r,c).isCovered())) {
				cout << "This field is already uncovered." << endl;
				continue;				//nic se nezmenilo -> vynechame tisk
			} else {
				try {
					board.uncover(l,r,c);					
				} catch (AccessForbiddenException e) {
					cout << e.errText << endl;
					cout << "Unmark the field firstly, use the \'M\' command." << endl;
					continue;
				}
			}
		} else {																		//prikaz = (OD/ZA)MASKOVAT
			try {
				board.setFieldMark(l,r,c, !(board(l,r,c).hasMark()));
			} catch (AccessForbiddenException e) {
				cout << e.errText << endl;
				continue;
			}
		}
		
		//tisk desky a poctu vlajecek
		cout << board << "Marks: " << board.getMarkedCount() << "/" << board.getMinesCount() << endl;
		
		//testy na konec hry
		if(uncover && board(l,r,c).hasMine()) {
			cout << "Oooops, you stepped on a mine which exploded!" << endl << "Sorry, you lost." << endl;
			break;
		}
		if(board.isCleared()) {
			cout << "Very well, mister!" << endl << "You won." << endl;
			break;
		}
	} // konec smycky

	cout << endl << " ***** QUIT *****" << endl;
	return 0;
}
