/* 
 * File:   	asserts.h
 * Created:	6.1.2010
 * Author: 	Petr Kubizňák
 * Purpose:	Tento soubor necht je inkludovan tam, kde se pouzivaji asserty, namisto inkludovani primo <cassert>.
 * 					Zde tyto asserty lze povolovat/blokovat globalne.
 */

#ifndef ASSERTS_H_
#define ASSERTS_H_

using namespace std;
//#define NDEBUG						//toto makro necht je u release verze odkomentovane
#include <cassert>

#endif /* ASSERTS_H_ */
