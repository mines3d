/* 
 * File:   main.cpp
 * Author: Petr Kubiznak,...
 */

#include "ui/console/console.h"
#include "ui/qt/qt_gui.h"
#include "exceptions/GeneralException.h"
#include <iostream>
#include <cstring>
using namespace std;

/* -------------------------------------------------------------------------- */

/* tiskne kratkou napovedu */
void printHelp() {
	cout << endl << "NAME" << endl;
	cout << "\tMines3D - Search mines in three dimensions!" << endl << endl;

	cout << "SYNOPSIS:" << endl;
	cout << "\tmines3d [OPTION]" << endl << endl;

	cout << "OPTIONS" << endl;
	cout << "\t-console" << endl << "\t\tRun in a console mode. Implicitly, the program starts in a graphic mode." << endl << endl;
	cout << "\t-help" << endl << "\t\tDisplays this help." << endl << endl;
	
	cout << "CREDITS" << endl;
	cout << "\t\302\251 Petr Kubiz\305\210\303\241k, Czech Technical University in Prague, 2010" << endl;
	cout << "\tCreated as a semestral project on subject Y36PJC." << endl << endl;
	cout << "\tThanks to Petra Mertl\303\255kov\303\241 for a theme suggestion and long-term support." << endl << endl;
}

/* -------------------------------------------------------------------------- */

int main(int argc, char *argv[]) {
	try {
		switch(argc) {
		case 1:
			cout << "Starting graphical mode." << endl;
			cout << "To run in a text mode, use argument \"-console\"." << endl;
			qtMain(argc, argv);
			break;
	
		case 2:
			if(!strcmp(argv[1], "-console")) { consoleMain(); break; }			//spusteni programu v console modu
			if(!strcmp(argv[1], "-help")) { printHelp(); break; }
	
		default:
			cerr << "Invalid calling seqence." << endl;
			cerr << "Call \"" << argv[0] << "\" to run the program." << endl;
			cerr << "Use \"-help\" argument to display help." << endl;
			return 1;
			break;
		}
	} catch(GeneralException e) {
		cerr << "Program terminated incorrectly with error message:" << endl
			<< e.errText << " (" << e.errCode << ")" << endl;
	} catch(...) {
		cerr << "Program terminated incorrectly with unknown error." << endl;
	}

  return 0;
}

