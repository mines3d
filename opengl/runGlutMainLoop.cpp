
#if defined(_WIN32) || defined(__WIN32__)
#	include <windows.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <GL/gl.h>


#include <GL/glut.h>
#include "trackball.h"
#include "readtex.h"
#include "runGlutMainLoop.h"
#include "../core/board.h"
#include "../threads/thread_controller.h"

#ifndef byte
#	define byte        unsigned char
#endif


#define glut_WINDOW_SIZE 500
#define glut_WINDOW_POS_X 500
#define glut_WINDOW_POS_Y 200
#define glut_WINDOW_TITLE "OpenGL GLUT - 3D miny"
#define glut_CUBE_SIZE 0.5f
#define glut_ANGLE 1.0f
#define TEXTURE_FILE "textury/all.sgi"
#define TEXTURE_OFFSET_UNIT_X 0.125f
#define TEXTURE_OFFSET_UNIT_Y 0.25f
#define ALPHA_CONST 0.8f

void (*drawMines)();

//void runGlutMainLoop(int x,int y,int z);

//void * runGlutMainLoop( void * args);

void init();
void display(void);
void reshape(int width,int height);
void keyboard_input(int key,int x,int y);
void idle(void);
void drawMines1(void);
void drawMine(int x,int y,int z);
void initLights(void);
void mouse_input(int button,int state,int x,int y);
void motion(int x,int y);
void displaySub(void);
void makeTextures(void);
void motion_pas(int x,int y);
void forceRedraw(void);
void app_exiting(void);


int * minesX;
int * minesY;
int * minesZ;

int w_width;
int w_height;

int win, subWin;

byte mode = 1;
byte update_nums = 0;
float zoom = 1.0f;

int cropX_min = 0,cropX_max;
int cropY_min = 0,cropY_max;
int cropZ_min = 0,cropZ_max;
int last_x,last_y,last_z;

float mouse_old_x = 0, mouse_old_y = 0;

//Board * board;
Thread_controller * thc;

float curQuat[4], incQuat[4];

const char * strX = "X: min    max";
const char * strY = "Y: min    max";
const char * strZ = "Z: min    max";

int selectXYZ = -1;

char update = 1;

GLfloat ambient [] = { 1.0f, 1.0f, 1.0f, 1.0f };

GLfloat diffuse [][4] =
{
    { 0.9f, 0.9f, 0.9f, 1.0f  },    // 0 light grey
    { 0.0f, 0.0f, 1.0f, 1.0f  },    // 1 blue
    { 0.0f, 1.0f, 0.0f, 1.0f  },    // 2 green
    { 1.0f, 0.0f, 0.0f, 1.0f  },    // 3 red
    { 0.0f, 0.0f, 0.5f, 1.0f  },    // 4 dark blue
    { 0.7f, 0.0f, 0.7f, 1.0f  },    // 5 purple
    { 0.0f, 0.5f, 0.0f, 1.0f  },    // 6 dark green
    { 0.5f, 0.0f, 0.0f, 1.0f  },    // 7 dark red
    { 0.32f, 0.32f, 0.32f, 1.0f  },    // 8 grey
    { 0.0f, 0.0f, 0.0f, 0.0f  }     // 9 black
};

GLfloat sunDirection [] = { 0.0f, -1.0f, 0.0f }; 
GLfloat sunPosition [3];

//void runGlutMainLoop(int x,int y,int z )

void forceRedraw(void)
{
    glFlush();
    glutPostRedisplay();
    //display();
    
}

void app_exiting(void)
{
    update = 0;
}

void * runGlutMainLoop( void * args )
{
	char *argval = new char [1];
	char **argv = new char* [1];
	argv[0] = argval;
	argval[0] = '\0';
	int argc = 1;
	
    thc = (Thread_controller *)args;
    
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(glut_WINDOW_SIZE,glut_WINDOW_SIZE);
    glutInitWindowPosition(glut_WINDOW_POS_X,glut_WINDOW_POS_Y);
    win = glutCreateWindow(glut_WINDOW_TITLE);
    w_height = w_width = glut_WINDOW_SIZE;
    
    drawMines = drawMines1;
    
    init();
    initLights();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);
    
    glutMotionFunc(motion);
    glutSpecialFunc(keyboard_input);
    
    trackball(curQuat,0.0,0.0,0.0,0.0);
    
    subWin = glutCreateSubWindow(win, 0, 0, 340, 60);
    glutDisplayFunc(displaySub);
    glutMouseFunc(mouse_input);
    glutPassiveMotionFunc(motion_pas);
    
    thc->ptf = &forceRedraw;
    thc->ptf_end = &app_exiting;
    
    glutMainLoop();
    return 0;
}

void init()
{
    
    
    glPolygonMode( GL_FRONT, GL_FILL );
	glPolygonMode( GL_BACK, GL_LINE );
	glCullFace( GL_BACK );
	
	minesX = &thc->dim[0];
	minesY = &thc->dim[1];
	minesZ = &thc->dim[2];
	
	sunPosition[0] = 0.0f;
	sunPosition[1] = 0.0f;
	sunPosition[2] = (*minesY) * 8 * glut_CUBE_SIZE;
	
	cropX_max = last_x = *minesX;
	cropY_max = last_y = *minesY;
	cropZ_max = last_z = *minesZ;
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE); 
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST); 
    
    if(!LoadRGBMipmapsUserAlpha(TEXTURE_FILE, GL_RGBA,ALPHA_CONST)) {
        printf("Error: couldn't load texture image\n");
    }
	
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING); 
    glEnable(GL_DEPTH_TEST);
}

void initLights(void)
{
    glEnable(GL_LIGHT0);
    glLightf(GL_LIGHT0,GL_SPOT_CUTOFF,180);
    
    glLightfv(GL_LIGHT0,GL_POSITION, sunPosition );
    glLightfv(GL_LIGHT0,GL_SPOT_DIRECTION,sunDirection);    
    
}

void displaySub(void)
{
    glClearColor(0.2, 0.2, 0.2, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1, 1, 1);
    glRasterPos2f(-0.95,0.5);
    int i,len;
    char * pom = (char*)malloc(25*sizeof(char));
    if (cropX_min <= 9) sprintf(pom," %i",cropX_min);
    else sprintf(pom,"%i",cropX_min);
    len = strlen(pom);
    for(i=0;i<7;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, strX[i]);    
    for(i=0;i<len;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15,pom[i]);
    
    if (cropX_max <= 9) sprintf(pom," %i",cropX_max);
    else sprintf(pom,"%i",cropX_max);
    len = strlen(pom);
      
    for(i=7;i<14;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, strX[i]);
    for(i=0;i<len;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15,pom[i]);
        
    glRasterPos2f(-0.95,-0.1);    
    if (cropY_min <= 9) sprintf(pom," %i",cropY_min);
    else sprintf(pom,"%i",cropY_min);
    len = strlen(pom);
    for(i=0;i<7;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, strY[i]);    
    for(i=0;i<len;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15,pom[i]);
    
    if (cropY_max <= 9) sprintf(pom," %i",cropY_max);
    else sprintf(pom,"%i",cropY_max);
    len = strlen(pom);    
    for(i=7;i<14;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, strY[i]);
    for(i=0;i<len;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15,pom[i]);        
    
    glRasterPos2f(-0.95,-0.7); 
    if (cropZ_min <= 9) sprintf(pom," %i",cropZ_min);
    else sprintf(pom,"%i",cropZ_min);
    len = strlen(pom);
    for(i=0;i<7;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, strZ[i]);    
    for(i=0;i<len;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15,pom[i]);
    
    if (cropZ_max <= 9) sprintf(pom," %i",cropZ_max);
    else sprintf(pom,"%i",cropZ_max);
    len = strlen(pom);    
    for(i=7;i<14;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, strZ[i]);
    for(i=0;i<len;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15,pom[i]);

    
    

    if (selectXYZ == 8) glColor3f(1, 0, 0);
    else glColor3f(1, 1, 0);
    glRasterPos2f(-0.45,0.5);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15,'+');
    
    if (selectXYZ == 9) glColor3f(1, 0, 0);
    else glColor3f(1, 1, 0);
    glRasterPos2f(-0.38,0.5);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15,'-');
    
    if (selectXYZ == 10) glColor3f(1, 0, 0);
    else glColor3f(1, 1, 0);
    glRasterPos2f(0.03,0.5);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15,'+');
    
    if (selectXYZ == 11) glColor3f(1, 0, 0);
    else glColor3f(1, 1, 0);
    glRasterPos2f(0.10,0.5);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15,'-');
    
    
    if (selectXYZ == 4) glColor3f(1, 0, 0);
    else glColor3f(1, 1, 0);
    glRasterPos2f(-0.45,-0.1);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15,'+');

    if (selectXYZ == 5) glColor3f(1, 0, 0);
    else glColor3f(1, 1, 0);    
    glRasterPos2f(-0.38,-0.1);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15,'-');

    if (selectXYZ == 6) glColor3f(1, 0, 0);
    else glColor3f(1, 1, 0);    
    glRasterPos2f(0.03,-0.1);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15,'+');

    if (selectXYZ == 7) glColor3f(1, 0, 0);
    else glColor3f(1, 1, 0);    
    glRasterPos2f(0.10,-0.1);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15,'-');
    
    
    if (selectXYZ == 0) glColor3f(1, 0, 0);
    else glColor3f(1, 1, 0);  
    glRasterPos2f(-0.45,-0.7);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15,'+');

    if (selectXYZ == 1) glColor3f(1, 0, 0);
    else glColor3f(1, 1, 0);      
    glRasterPos2f(-0.38,-0.7);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15,'-');

    if (selectXYZ == 2) glColor3f(1, 0, 0);
    else glColor3f(1, 1, 0);      
    glRasterPos2f(0.03,-0.7);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15,'+');

    if (selectXYZ == 3) glColor3f(1, 0, 0);
    else glColor3f(1, 1, 0);      
    glRasterPos2f(0.10,-0.7);
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15,'-');

    if (selectXYZ == 12) glColor3f(1, 0, 0);
    else glColor3f(1, 1, 0);     
    glRasterPos2f(0.2,0.5);
    sprintf(pom,"SWITCH MODE %i",mode);
    len = strlen(pom);    
    for(i=0;i<len;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15,pom[i]);
        
    if (selectXYZ == 13) glColor3f(1, 0, 0);
    else glColor3f(1, 1, 0);     
    glRasterPos2f(0.2,-0.1);
    const char * upn_str;
    if (update_nums) upn_str = "ON";
    else upn_str = "OFF";
    
    sprintf(pom,"UPDATE NUMS %s",upn_str);
    len = strlen(pom);    
    for(i=0;i<len;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15,pom[i]);
    
    
    
    free(pom);
    
    glutSwapBuffers();
}

void display(void)
{
    if (update == 0)
    {
        pthread_exit((void *)0);
    }
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    
    thc->mutex_lock();
    
    if (update == 0) 
    {
        thc->mutex_unlock();
        pthread_exit((void *)0);
    }

    if ( last_x != *minesX || last_y != *minesY || last_z != *minesZ )
    {
        cropX_max = last_x = *minesX;
	    cropY_max = last_y = *minesY;
	    cropZ_max = last_z = *minesZ;
	    glutPostWindowRedisplay(win);
    }
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    int m = *minesX;
    if (*minesY > m) m = *minesY;
    if (*minesZ > m) m = *minesZ;
    
    
    
    gluLookAt( 0.0, 0.0  , glut_CUBE_SIZE * 6 * m * zoom,
        0.0,0.0,0.0,
        0.0, 1.0, 0.0
    );
    
    glPushMatrix();

    if (thc->b != 0) drawMines();
    
    
    
    glMaterialfv(GL_FRONT,GL_AMBIENT,ambient);
    glMaterialfv(GL_FRONT,GL_DIFFUSE,diffuse[0]);
    
    glPopMatrix();
    
    thc->mutex_unlock();
    
    
    glutSwapBuffers();
    glutPostRedisplay();
    
    
}

void reshape(int width,int height)
{
    if (update == 0)
    {
        pthread_exit((void *)0);
    }
    w_width = width;
    w_height = height;
    
    glViewport(0,0,width,height);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0,(GLdouble)width/height,0.1,200.0);
    
    thc->mutex_lock();
    
    if (update == 0) 
    {
        thc->mutex_unlock();
        pthread_exit((void *)0);
        return;
    }
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    int m = *minesX;
    if (*minesY > m) m = *minesY;
    if (*minesZ > m) m = *minesZ;
    
    thc->mutex_unlock();
    
    gluLookAt( 0.0, 0.0  , glut_CUBE_SIZE * 6 * m * zoom,
        0.0,0.0,0.0,
        0.0, 1.0, 0.0
    );
    
}

void idle(void)
{
    glutPostRedisplay();    
}

void keyboard_input(int key,int x,int y)
{
    if (key == GLUT_KEY_LEFT) zoom += 0.025f;
    if (key == GLUT_KEY_RIGHT) zoom -= 0.025f;
    
}

void mouse_input(int button,int state,int x,int y)
{
    
    if( button == GLUT_LEFT_BUTTON )
    {
        if (state == GLUT_DOWN)
        {
            switch( selectXYZ )
            {
                case 12:
                    ++(mode %= 4);
                    break;    
                case 13:
                    if (update_nums) update_nums = 0;
                    else update_nums = 1;
                    break;    
                case 8:
                    if (cropX_min < *minesX-1) cropX_min += 1;
                    break;
                case 9:
                    if (cropX_min > 0) cropX_min -= 1;
                    break;
                case 10:
                    if (cropX_max < *minesX) cropX_max += 1;
                    break;
                case 11:
                    if (cropX_max > 0) cropX_max -= 1;
                    break;                    
                case 4:
                    if (cropY_min < *minesY-1) cropY_min += 1;
                    break;
                case 5:
                    if (cropY_min > 0) cropY_min -= 1;
                    break;
                case 6:
                    if (cropY_max < *minesY) cropY_max += 1;
                    break;
                case 7:
                    if (cropY_max > 0) cropY_max -= 1;
                    break;
                case 0:
                    if (cropZ_min < *minesZ-1) cropZ_min += 1;
                    break;
                case 1:
                    if (cropZ_min > 0) cropZ_min -= 1;
                    break;
                case 2:
                    if (cropZ_max < *minesZ) cropZ_max += 1;
                    break;
                case 3:
                    if (cropZ_max > 0) cropZ_max -= 1;
                    break;                       
                    
            }
            
        }
    }
    glutPostWindowRedisplay(win);  
    
}

void motion(int x, int y)
{
    int x1,y1;
    x1 = x;
    y1 = y;
    if (mouse_old_x - x > w_width / 100  ) x = (int)mouse_old_x - w_width / 100;
    if (mouse_old_x - x < -w_width / 100) x = (int)mouse_old_x + w_width / 100;
    
    if (mouse_old_y - y > w_height / 100) y = (int)mouse_old_y - w_height/ 100;
    if (mouse_old_y - y < -w_height / 100) y = (int)mouse_old_y + w_height / 100;
    
    trackball(incQuat, (2.0 * mouse_old_x - w_width) / w_width,
        (w_height - 2.0 * mouse_old_y) / w_height,
        (2.0 * x - w_width) / w_width,
        (w_height - 2.0 * y) / w_height );
        
        
    add_quats(incQuat,curQuat,curQuat);
    
    mouse_old_x = x1;
    mouse_old_y = y1;
       
    //glutPostRedisplay();  

    
}

void drawMines1(void)
{
    int i,j,k;
    float m[4][4]; 
    
    glPushMatrix();
    
    
    
    build_rotmatrix(m,curQuat);
    glMultMatrixf(&m[0][0]);
    
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    
    glBegin(GL_LINES);
        glColor3f(1.0, 0.0, 0.0);
        glVertex3f(0.0,0.0,0.0);
        glVertex3f(glut_CUBE_SIZE * 4 * *minesX,0.0,0.0);
        
        glVertex3f(glut_CUBE_SIZE * 4 * *minesX,0.0,0.0);
        glVertex3f(glut_CUBE_SIZE * 4 * *minesX - 2,2.0,0.0);
        glVertex3f(glut_CUBE_SIZE * 4 * *minesX,0.0,0.0);
        glVertex3f(glut_CUBE_SIZE * 4 * *minesX - 2,-2.0,0.0);
        
        glColor3f(0.0, 1.0, 0.0);
        glVertex3f(0.0,0.0,0.0);
        glVertex3f(0.0,glut_CUBE_SIZE * 4 * *minesY,0.0);
        
        glVertex3f(0.0,glut_CUBE_SIZE * 4 * *minesY,0.0);
        glVertex3f(2.0,glut_CUBE_SIZE * 4 * *minesY-2,0.0);
        glVertex3f(0.0,glut_CUBE_SIZE * 4 * *minesY,0.0);
        glVertex3f(-2.0,glut_CUBE_SIZE * 4 * *minesY-2,0.0);
        
        glColor3f(0.0, 0.0, 1.0);
        glVertex3f(0.0,0.0,0.0);
        glVertex3f(0.0,0.0,glut_CUBE_SIZE * 4 * *minesZ);
        
        glVertex3f(0.0,0.0,glut_CUBE_SIZE * 4 * *minesZ);
        glVertex3f(2.0,0.0,glut_CUBE_SIZE * 4 * *minesZ-2);
        glVertex3f(0.0,0.0,glut_CUBE_SIZE * 4 * *minesZ);
        glVertex3f(-2.0,0.0,glut_CUBE_SIZE * 4 * *minesZ-2);
    
    
    glEnd();
    
    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    
    glEnable (GL_BLEND);
    glDepthMask (GL_FALSE); 
    glBlendFunc (GL_SRC_ALPHA,GL_ONE_MINUS_SRC_COLOR); 
    
    //if (mode > 2) glTranslated(-4 * glut_CUBE_SIZE * thc->selected[1], 4 * glut_CUBE_SIZE * thc->selected[2],4 * glut_CUBE_SIZE * thc->selected[0] );
    //else glTranslated( -2 * (*minesX-1) * glut_CUBE_SIZE,2 * (*minesY-1) * glut_CUBE_SIZE,2 * (*minesZ-1) * glut_CUBE_SIZE  );
    
    if (mode > 2) glTranslated(-4 * glut_CUBE_SIZE * thc->selected[2], 4 * glut_CUBE_SIZE * thc->selected[1],4 * glut_CUBE_SIZE * thc->selected[0] );
    else glTranslated( -2 * (*minesX-1) * glut_CUBE_SIZE,2 * (*minesY-1) * glut_CUBE_SIZE,2 * (*minesZ-1) * glut_CUBE_SIZE  );
    
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    
    for (i = 0;i < *minesX ;i++  )
    {
        if ((i < thc->selected[1]-2 || i > thc->selected[1]+2) && mode > 2 ) continue;
        
        if (i < cropX_min || i > cropX_max) continue;
        
        for ( j = 0;j < *minesZ; j++ )
        {
            if ((j < thc->selected[0]-2 || j > thc->selected[0]+2) && mode > 2 ) continue;
            if (j < cropZ_min || j > cropZ_max) continue;
            for ( k = 0;k < *minesY; k++ )
            {
                if ((k < thc->selected[2]-2 || k > thc->selected[2]+2) && mode > 2 ) continue;
                if (k < cropY_min || k > cropY_max) continue;
                glPushMatrix();
                               
                //glTranslated( 4 * i * glut_CUBE_SIZE, 4 * k * glut_CUBE_SIZE, -4 * j * glut_CUBE_SIZE );
                
                glTranslated( 4 * k * glut_CUBE_SIZE, -4 * i * glut_CUBE_SIZE, -4 * j * glut_CUBE_SIZE );
                
                glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
                glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse[0]);
                
                if ((thc->selected[0] == j && thc->selected[1] == i && thc->selected[2] == k) || thc->selected[0] == -1 || mode % 2  == 1)
                {
                    glDepthMask (GL_TRUE);
                    glDisable (GL_BLEND);
                    
                    if ((thc->selected[3] == j && thc->selected[4] == i && thc->selected[5] == k) ) 
                        glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse[8]);
                    drawMine(j,i,k);
                    glEnable (GL_BLEND);
                    glDepthMask (GL_FALSE); 
                    glBlendFunc (GL_SRC_ALPHA,GL_ONE_MINUS_SRC_COLOR); 
                    
                }
                else
                {
                    drawMine(j,i,k);
                }
                glPopMatrix();
                
            }
        }
    }
            
    glPopAttrib();   
    
    glDepthMask (GL_TRUE);
    glDisable (GL_BLEND); 
    
    glPopMatrix();
    glFlush();
    
    
}

void drawMine(int x,int y,int z)
{
    int val;
    
    //glEnable(GL_TEXTURE_2D);
    //glDisable( GL_LIGHTING );
    
    const Field & f = thc->b->getField(x,y,z);
    
    val = 29;
    int total_offsetX = 0;
    int total_offsetY = 3;
    
    if ( f.hasMark() )
    {
        val = 26;
        goto calc_offset;
    }
    if ( !f.isCovered() )
    {
        if ( f.hasMine() )
        {
            val = 27;
            goto calc_offset;
        }
        if (update_nums)
        {        
            val = f.getRemainingNeighboursCnt() - 1;
            if (val == -1 ) return;
            if (val < -1 ) val = 28;
        }
        else
        {
            val = f.getNeighboursCnt() - 1;
            if (val == -1 ) return;
        }
    }
    
calc_offset:
    
    total_offsetX = val % 8;
    total_offsetY -= (val / 8) % 4;
    
    
    glBegin(GL_QUADS);
    
        // front
        
        glNormal3f(0.0f, 0.0f, 1.0f);
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY );
        glVertex3f( -glut_CUBE_SIZE,-glut_CUBE_SIZE, glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X + TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY );
        glVertex3f( glut_CUBE_SIZE,-glut_CUBE_SIZE, glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X + TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY + TEXTURE_OFFSET_UNIT_Y);
        glVertex3f( glut_CUBE_SIZE,glut_CUBE_SIZE, glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X,TEXTURE_OFFSET_UNIT_Y * total_offsetY + TEXTURE_OFFSET_UNIT_Y);
        glVertex3f( -glut_CUBE_SIZE,glut_CUBE_SIZE, glut_CUBE_SIZE );
        
        // back
        glNormal3f(0.0f, 0.0f, -1.0f);
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY );
        glVertex3f( glut_CUBE_SIZE,-glut_CUBE_SIZE, -glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X + TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY );
        glVertex3f( -glut_CUBE_SIZE,-glut_CUBE_SIZE, -glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X + TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY + TEXTURE_OFFSET_UNIT_Y);
        glVertex3f( -glut_CUBE_SIZE,glut_CUBE_SIZE, -glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X,TEXTURE_OFFSET_UNIT_Y * total_offsetY + TEXTURE_OFFSET_UNIT_Y);
        glVertex3f( glut_CUBE_SIZE,glut_CUBE_SIZE, -glut_CUBE_SIZE );
        
        // left
        glNormal3f(-1.0f, 0.0f, 0.0f);
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY );
        glVertex3f( -glut_CUBE_SIZE,-glut_CUBE_SIZE, -glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X + TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY );
        glVertex3f( -glut_CUBE_SIZE,-glut_CUBE_SIZE, glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X + TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY + TEXTURE_OFFSET_UNIT_Y);
        glVertex3f( -glut_CUBE_SIZE,glut_CUBE_SIZE, glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X,TEXTURE_OFFSET_UNIT_Y * total_offsetY + TEXTURE_OFFSET_UNIT_Y);
        glVertex3f( -glut_CUBE_SIZE,glut_CUBE_SIZE, -glut_CUBE_SIZE );        
        
        // right
        glNormal3f(1.0f, 0.0f, 0.0f);
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY );
        glVertex3f( glut_CUBE_SIZE,-glut_CUBE_SIZE, glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X + TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY );
        glVertex3f( glut_CUBE_SIZE,-glut_CUBE_SIZE, -glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X + TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY + TEXTURE_OFFSET_UNIT_Y);
        glVertex3f( glut_CUBE_SIZE,glut_CUBE_SIZE, -glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X,TEXTURE_OFFSET_UNIT_Y * total_offsetY + TEXTURE_OFFSET_UNIT_Y);
        glVertex3f( glut_CUBE_SIZE,glut_CUBE_SIZE, glut_CUBE_SIZE );
        
        // bottom
        glNormal3f(0.0f, -1.0f, 0.0f);
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY );
        glVertex3f( -glut_CUBE_SIZE,-glut_CUBE_SIZE, glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X + TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY );
        glVertex3f( -glut_CUBE_SIZE,-glut_CUBE_SIZE, -glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X + TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY + TEXTURE_OFFSET_UNIT_Y);
        glVertex3f( glut_CUBE_SIZE,-glut_CUBE_SIZE, -glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X,TEXTURE_OFFSET_UNIT_Y * total_offsetY + TEXTURE_OFFSET_UNIT_Y);
        glVertex3f( glut_CUBE_SIZE,-glut_CUBE_SIZE, glut_CUBE_SIZE );  
        
        // top
        glNormal3f(0.0f, 1.0f, 0.0f);
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY );
        glVertex3f( -glut_CUBE_SIZE,glut_CUBE_SIZE, glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X + TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY );
        glVertex3f( glut_CUBE_SIZE,glut_CUBE_SIZE, glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X + TEXTURE_OFFSET_UNIT_X, TEXTURE_OFFSET_UNIT_Y * total_offsetY + TEXTURE_OFFSET_UNIT_Y);
        glVertex3f( glut_CUBE_SIZE,glut_CUBE_SIZE, -glut_CUBE_SIZE );
        
        glTexCoord2f(total_offsetX * TEXTURE_OFFSET_UNIT_X,TEXTURE_OFFSET_UNIT_Y * total_offsetY + TEXTURE_OFFSET_UNIT_Y);
        glVertex3f( -glut_CUBE_SIZE,glut_CUBE_SIZE, -glut_CUBE_SIZE );  
        
        
    
    
    glEnd(); 
    
    //glDisable(GL_TEXTURE_2D);
    //glEnable( GL_LIGHTING );
       

}

void motion_pas(int x,int y)
{
    
    if (y < 54 && y > 37)
    {
        if (x < 196)
        {
            if (x > 184 ) 
            {
                selectXYZ = 3;
                return;
            }
            if (x > 174 )
            {
                selectXYZ = 2;
                return;
            }
        }
        if (x < 115)
        {
            if (x > 104 ) 
            {
                selectXYZ = 1;
                return;
            }
            if (x > 94 )
            {
                selectXYZ = 0;
                return;
            }
        }
    }
    
    
    if (y < 37 && y > 19)
    {
        if (x < 336 && x > 200)
        {
            selectXYZ = 13;
            return;
        }
        
        if (x < 196)
        {
            if (x > 184 ) 
            {
                selectXYZ = 7;
                return;
            }
            if (x > 174 )
            {
                selectXYZ = 6;
                return;
            }
        }
        if (x < 115)
        {
            if (x > 104 ) 
            {
                selectXYZ = 5;
                return;
            }
            if (x > 94 )
            {
                selectXYZ = 4;
                return;
            }
        }
    }
    
    
    if (y < 19 && y > 2)
    {
        if (x < 320 && x > 200)
        {
            selectXYZ = 12;
            return;
        }
        
        if (x < 196)
        {
            if (x > 184 ) 
            {
                selectXYZ = 11;
                return;
            }
            if (x > 174 )
            {
                selectXYZ = 10;
                return;
            }
        }
        if (x < 115)
        {            
            if (x > 104 ) 
            {
                selectXYZ = 9;
                return;
            }
            if (x > 94 )
            {
                selectXYZ = 8;
                return;
            }
        }
    }
    
    selectXYZ = -1;

    
    
}












