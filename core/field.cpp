/*
 * File:   board.cpp
 * Author: Petr Kubiznak
 */

#include "field.h"
#include "../exceptions/GeneralException.h"
#include "../exceptions/AccessForbiddenException.h"

/* -------------------------------------------------------------------------- */

/* implicitni konstruktor (vytvori zakryte pole bez miny a oznaceni */
Field::Field(unsigned short int val) {
  data = val;
}

/* -------------------------------------------------------------------------- */

Field::~Field(void) {
}

/* -------------------------------------------------------------------------- */

/* vraci true, pokud je nastaven mine-bit */
bool Field::hasMine(void) const {
  return data & MASK_MINE;
}

/* -------------------------------------------------------------------------- */

/* danemu poli nastavi mine-bit, zda-li je pritomna mina */
void Field::setMine(bool setVal) {
  data = (data & (~MASK_MINE)) | (setVal ? MASK_MINE : 0);
}

/* -------------------------------------------------------------------------- */

/* vraci true, pokud je nastaven mark-bit (pole oznaceno jako zaminovane) */
bool Field::hasMark(void) const {
  return data & MASK_MARK;
}

/* -------------------------------------------------------------------------- */

/* danemu poli nastavi mark-bit podle hodnoty setVal */
void Field::setMark(bool setVal) {
	if(!isCovered()) throw AccessForbiddenException("Field is already uncovered, cannot be marked.");
	data = (data & (~MASK_MARK)) | (setVal ? MASK_MARK : 0);
}

/* -------------------------------------------------------------------------- */

/* vraci true, pokud je zadane pole neodkryte - je nastaven covered-bit */
bool Field::isCovered(void) const {
  return data & MASK_COVERED;
}

/* -------------------------------------------------------------------------- */

/* nastavi covered-bit */
void Field::setCovered(bool setVal) {
  data = (data & ~MASK_COVERED) | (setVal ? MASK_COVERED : 0);
}

/* -------------------------------------------------------------------------- */

int Field::getNeighboursCnt(void) const {
	if(isCovered()) throw AccessForbiddenException("");
	else return (int) ((data & MASK_NEIGHBOURS) >> MASK_NEIGHBOURS_SHIFT);
}

/* -------------------------------------------------------------------------- */

void Field::setNeighboursCnt(int count) {
	count = max(count, 0);
	data = (data & ~MASK_NEIGHBOURS) | ((count << MASK_NEIGHBOURS_SHIFT) & MASK_NEIGHBOURS);
}

/* -------------------------------------------------------------------------- */

int Field::getRemainingNeighboursCntNE(void) const {
	int sign = (data & MASK_REMAINING_SIGN ? -1 : 1);
	return sign * ((int) ((data & MASK_REMAINING) >> MASK_REMAINING_SHIFT));
}

/* -------------------------------------------------------------------------- */

int Field::getRemainingNeighboursCnt(void) const {
	if(isCovered()) throw AccessForbiddenException("");
	else return getRemainingNeighboursCntNE();
}

/* -------------------------------------------------------------------------- */

void Field::setRemainingNeighboursCnt(int count) {
	data = (data & ~MASK_REMAINING_SIGN) | (count < 0 ? MASK_REMAINING_SIGN : 0);
	count = (count >= 0 ? count : -count);
	data = (data & ~MASK_REMAINING) | ((count << MASK_REMAINING_SHIFT) & MASK_REMAINING);
}

/* -------------------------------------------------------------------------- */

void Field::fixRemainingNeighboursCnt(int increment) {
	setRemainingNeighboursCnt(getRemainingNeighboursCntNE() + increment);
}

/* -------------------------------------------------------------------------- */

/* odkryje zadane pole a vrati pocet min v jeho okoli */
int Field::uncover(void) {
  setCovered(false);
  return getNeighboursCnt();
}

/* -------------------------------------------------------------------------- */

/* pretypovani pole na int vraci vnitrni reprezentaci - hodnotu var "data" */
Field::operator int (void) const {
  return (int) data;
}
