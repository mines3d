/* 
 * File:   board.cpp
 * Author: Petr Kubiznak
 */

#include "board.h"
#include "../exceptions/GeneralException.h"
#include "../exceptions/OutOfBoundsException.h"
#include "../exceptions/AccessForbiddenException.h"
#include "../asserts.h"
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include <cstdio>
using namespace std;

/* -------------------------------------------------------------------------- */

#define FIELD_WIDTH			2				//sirka, na kterou se tiskne jedno policko do konzole

/* -------------------------------------------------------------------------- */

/* vytvori 3d hraci pole o rozmerech rows x cols x layers a rozlozi do nej zadany pocet min,
 * tech muze byt maximalne 1/2 z celkoveho poctu policek */
Board::Board(int rows, int cols, int layers, int minesCount) {
                               
                 
  assert(rows > 0);
  assert(cols > 0);
  assert(layers > 0);
  
  this->layers = layers;
  this->rows = rows;
  this->cols = cols;
  this->minesCount = minesCount;

  //alokace pameti
  this->board = new Field ** [layers];
  for(int l=0; l<layers; l++) {
    this->board[l] = new Field * [rows];
    for(int r=0; r<rows; r++) {
      this->board[l][r] = new Field[cols];
    }
  }

  //rozmisteni min
  if(this->placeMines() != 0) throw GeneralException(ERR_DEFAULT, "Too many mines, use lesser number.");
  //vypocet sousednich min
  this->numberBoard();
  
}

/* -------------------------------------------------------------------------- */

Board::~Board(void) {
  for(int l=0; l<this->layers; l++) {
    for(int r=0; r<this->rows; r++) {
      delete [] this->board[l][r];
    }
    delete [] this->board[l];
  }
  delete [] this->board;
}

/* -------------------------------------------------------------------------- */

/* vraci pocet vrstev */
int Board::getLayersCount(void) const {
	return layers;
}

/* -------------------------------------------------------------------------- */

/* vraci pocet radku */
int Board::getRowsCount(void) const {
	return rows;
}

/* -------------------------------------------------------------------------- */

/* vraci pocet sloupcu */
int Board::getColsCount(void) const {
	return cols;
}

/* -------------------------------------------------------------------------- */

/* vraci pocet min v celem poli */
int Board::getMinesCount(void) const {
	return minesCount;
}

/* -------------------------------------------------------------------------- */

Field & Board::getFieldRW(int layer, int row, int col) const {
	if(! ((layer >= 0 && layer < this->layers) && (row >= 0 && row < this->rows) &&
			(col >= 0 && col < this->cols)) ) {
		char str[100];
		sprintf(str, "Invalid coordinates [%d, %d, %d].", layer, row, col);
		throw OutOfBoundsException(str);
	}
	return board[layer][row][col];	
}

/* -------------------------------------------------------------------------- */

/* vraci referenci na pole dane souradnicemi
 * v pripade neplatnych souradnic vyhazuje vyjimku ERR_OUT_OF_BOUNDS */
const Field & Board::getField(int layer, int row, int col) const {
	return (const Field &)getFieldRW(layer, row, col);
}

/* -------------------------------------------------------------------------- */

/* ternarni operator () - getter pro ziskani pole na danych souradnicich (vola getField)
 * v pripade neplatnych souradnic vyhazuje vyjimku ERR_OUT_OF_BOUNDS */
const Field & Board::operator () (int layer, int row, int col) const {
  return getField(layer, row, col);
}

/* -------------------------------------------------------------------------- */

/* tiskne cele hraci pole (pro debug) */
void Board::print(void) const {
  for(int l=0; l<this->layers; l++) {
    for(int r=0; r<this->rows; r++) {
      for(int c=0; c<this->cols; c++)
        cout << setw(3) << setfill('_') << (int) this->board[l][r][c] << setfill(' ') << " ";
      cout << endl;
    }
    cout << endl;
  }
}

/* -------------------------------------------------------------------------- */

/* pretizeny operator vystupu - tiskne cele pole v hernim modu */
ostream & operator << (ostream &os, const Board &board) {
  for(int l=0; l<board.layers; l++) {
    os << "Layer " << l << ":" << endl << setw(FIELD_WIDTH+2) << "";
    for(int c=0; c<board.cols; c++) os << setw(FIELD_WIDTH) << c << "|";
    os << endl;
    for(int r=0; r<board.rows; r++) {
      os << setw(FIELD_WIDTH) << r << ": ";
      for(int c=0; c<board.cols; c++) {
        const Field &f = board(l,r,c);
        if(f.isCovered()) os << setw(FIELD_WIDTH) << (f.hasMark() ? setfill('#') : setfill('_')) << "" << " " << setfill(' ');
        else if(f.hasMine()) os << setw(FIELD_WIDTH) << setfill('!') << "" << " " << setfill(' ');
        else {
        	int neighbours = f.getNeighboursCnt();
        	if(neighbours) os << setfill('_') << setw(FIELD_WIDTH) << neighbours << setfill(' ') << " ";
        	else os << setw(FIELD_WIDTH) << "" << " ";
        }
      }
      os << endl;
    }
    os << endl;
  }
  return os;
}

/* -------------------------------------------------------------------------- */

/* rozmisti miny po hracim poli
 * vraci 0 pri uspechu nebo 1, ma-li byt rozmisteno prilis mnoho min */
int Board::placeMines(void) {
  int minesPlaced = 0;
  if(this->minesCount > layers*rows*cols/2) return 1;
  
  srand(time(NULL));      //randomize

  while(minesPlaced < this->minesCount) {
    Field &field = getFieldRW(rand() % this->layers, rand() % this->rows, rand() % this->cols);
    if(!field.hasMine()) {
      field.setMine(true);
      minesPlaced++;
    }
  }

  return 0;
}

/* -------------------------------------------------------------------------- */

/* ocisluje policka podle pritomnosti min na sousednich polich */
int Board::numberBoard(void) {
  // pro vsechna pole zjistime pocet min v jejich okoli (vc. sebe sama) a tyto pocty v nich ulozime
  for(int l=0; l<this->layers; l++) {
    for(int r=0; r<this->rows; r++) {
      for(int c=0; c<this->cols; c++) {

        int cnt = 0;
        // projdeme sousedni policka (neighbours) (vc. tohoto) a za kazdou minu inkrementujeme >pocet "sousednich" min< v tomto poli
        for(int nl=-1; nl<=1; nl++) {
          for(int nr=-1; nr<=1; nr++) {
            for(int nc=-1; nc<=1; nc++) {
              try {
                cnt += getField(l+nl, r+nr, c+nc).hasMine();    //pri neplatnych souradnicich vyhazuje vyjimku, kterou zde ignorujeme
              } catch(OutOfBoundsException) {
                //neni chyba
              }
            }
          }
        }
        this->board[l][r][c].setNeighboursCnt(cnt);
        this->board[l][r][c].setRemainingNeighboursCnt(cnt);

      }
    }
  }
  return 0;
}

/* -------------------------------------------------------------------------- */

void Board::setFieldMark(int layer, int row, int col, bool value) {
	Field & f = getFieldRW(layer, row, col);
	if(f.hasMark() != value) {
		f.setMark(value);
		int inc = (value ? -1 : 1);
		
        // projdeme sousedni policka (neighbours) (vc. tohoto) a za kazdou minu
		// (in/de)krementujeme >zbyvajici pocet "sousednich" min< v danem poli
        for(int nl=-1; nl<=1; nl++) {
        	for(int nr=-1; nr<=1; nr++) {
        		for(int nc=-1; nc<=1; nc++) {
        			try {
        				getFieldRW(layer+nl, row+nr, col+nc).fixRemainingNeighboursCnt(inc);
        			} catch(OutOfBoundsException) {
        				//pole neexistuje
        			}
        		}
        	}
        }		
	}
}

/* -------------------------------------------------------------------------- */

/* odkryje zadane pole, nema-li v okoli zadne miny, odkryje i sousedni pole;
 * vraci true, pokud je pritomna mina */
bool Board::uncover(int layer, int row, int col) {
  Field &f = getFieldRW(layer, row, col);
  if(f.hasMark()) throw AccessForbiddenException("Field marked as mined cannot be uncovered.");
  if(!f.isCovered()) return f.hasMine();
  if(!f.uncover()) {      //pokud ma 0 sousedu - zadne miny v okoli (vcetne sebe sama), odkryjeme sousedni miny
    for(int l=-1; l<=1; l++)
      for(int r=-1; r<=1; r++)
        for(int c=-1; c<=1; c++) {
          if(l==0 && r==0 && c==0) continue;      //sebe sama uz neodkryvame
          try {
            uncover(layer+l, row+r, col+c);       //rekurzivne odkryjeme sousedni pole
          } catch(OutOfBoundsException) {
            // neni chyba
          } catch(AccessForbiddenException) {
          	// taky neni chyba (pole bylo zakryte, tak jsme ho proste neodkryli)
          }
        }
  }
  return f.hasMine();
}

/* -------------------------------------------------------------------------- */

/* vraci true, pokud jsou odkryta vsechna pole bez min */
bool Board::isCleared(void) const {
  for(int l=0; l<this->layers; l++) {
    for(int r=0; r<this->rows; r++) {
      for(int c=0; c<this->cols; c++)
      	if(board[l][r][c].isCovered() && !board[l][r][c].hasMine()) return false;
    }
  }
  return true;
}

/* -------------------------------------------------------------------------- */

/* vraci pocet poli oznacenych jako zaminovana */
int Board::getMarkedCount(void) const {
	int cnt=0;
  for(int l=0; l<this->layers; l++) {
    for(int r=0; r<this->rows; r++) {
      for(int c=0; c<this->cols; c++)
      	cnt += (board[l][r][c].hasMark());
    }
  }
  return cnt;
}

/* -------------------------------------------------------------------------- */

/* vraci herni mod podle rozmeru desky */
Board::GameMode Board::getMode(void) const {
	return getMode(layers, rows, cols, minesCount);
}

/* -------------------------------------------------------------------------- */

/* vraci herni mod podle zadanych rozmeru */
Board::GameMode Board::getMode(int layers, int rows, int cols, int mines) {
	if(layers==PRESETS[0][0] && rows==PRESETS[0][1] && cols==PRESETS[0][2] && mines==PRESETS[0][3]) return ModeRookie;
	if(layers==PRESETS[1][0] && rows==PRESETS[1][1] && cols==PRESETS[1][2] && mines==PRESETS[1][3]) return ModeAdvanced;
	if(layers==PRESETS[2][0] && rows==PRESETS[2][1] && cols==PRESETS[2][2] && mines==PRESETS[2][3]) return ModeSuicide;
	return ModeCustom;
}

/* -------------------------------------------------------------------------- */
