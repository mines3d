/** ************************************************************************************************
 *  @file board.h
 *  
 *  @brief One field of game board.
 *  
 *  Data structure representing one field, provides protected and unprotected access.
 *  
 *  @author Petr Kubiznak <kubiznak.petr@gmail.com>
 *  
 *  @since 2009-11-??
 *  
 **************************************************************************************************/

#ifndef _FIELD_H
#define	_FIELD_H

//**************************************************************************************************

// data bitfield masks
#define MASK_NEIGHBOURS   0x001F                    //!< Number of mines in the neighbourhood.
#define MASK_COVERED      0x0020                    //!< Covered flag.
#define MASK_MARK         0x0040                    //!< Mark flag.
#define MASK_MINE         0x0080                    //!< Mine flag.
#define MASK_UNKNOWN      0x0100                    //!< Unknown flag (second mark).
#define MASK_REMAINING    0x3E00                    //!< Number of NOT MARKED mines in the neighbourhood.
#define MASK_REMAINING_SIGN 0x4000                  //!< Sign of remaining mines count. Is set for minus.

// shifts of integer values
#define MASK_NEIGHBOURS_SHIFT  0
#define MASK_REMAINING_SHIFT   9

//**************************************************************************************************

/** Implements data structure representing one field of the board. */
class Field {
protected:
    unsigned short int data;                                                         //!< data of the structure - bitfield of flags and values

    /** Gets the number of UNMARKED mines arround this field.
     * @return Number of hiden mines according to user marks. The number does not
     * 	have to correspond to real unmarked number - it corresponds to what the USER THINKS
     *  that is the reality. */
    inline int getRemainingNeighboursCntNE(void) const;

    /** Sets the covered-bit.
     * @param setVal Use true to cover the field, false to uncover. */
    inline void setCovered(bool setVal);

public:
    /** Creates new object of the class.
     * @param val Bitfield content - be careful. Use default value to create implicit, covered field. */
    Field(unsigned short int val = MASK_COVERED);
    /** Frees allocated resources. */
    ~Field(void);

    /** Checks whether the field contains mine.
     * @return True if mine is present in the field, false otherwise. */
    bool hasMine(void) const;
    /** Sets, whether the field contains a mine.
     * @setVal Use true to set mine, false to unset. */
    void setMine(bool setVal);
    /** Checks whether the field is user-marked as mined.
     * @return True if field is marked, false otherwise. */
    bool hasMark(void) const;
    /** Sets the user-mark.
     * @setVal Use true to mark as mined, false to unmark.
     * @throw AccessForbiddenException if the field is already uncovered. */
    void setMark(bool setVal);
    /** Checks whether the field is covered.
     * @return True if it is covered, false if uncovered. */
    bool isCovered(void) const;
    /** Uncovers the field.
     * @return Number of mines hidden in field arround this one. */
    int uncover(void);
    /** Stores the number of mines hidden arround this field.
     * @param count Number of hiden mines. */
    void setNeighboursCnt(int count);
    /** Gets the number of mines arround this field.
     * @return Number of hiden mines.
     * @throw AccessForbiddenException if the field is still covered. */
    int getNeighboursCnt(void) const;
    /** Stores the number of UNMARKED mines hidden arround this field.
     * @param count Number of UNMARKED hiden mines (according to user marks). The number does not
     * 	have to correspond to real unmarked number - it corresponds to what the USER THINKS
     *  that is the reality. */
    void setRemainingNeighboursCnt(int count);
    /** Adds given increment to current value of UNMARKED mines hidden arround this field.
     * @param increment Value to add to the actual value. */
    void fixRemainingNeighboursCnt(int increment);
    /** Gets the number of UNMARKED mines arround this field.
     * @return Number of hiden mines according to user marks. The number does not
     * 	have to correspond to real unmarked number - it corresponds to what the USER THINKS
     *  that is the reality.
     * @throw AccessForbiddenException if the field is still covered. */
    int getRemainingNeighboursCnt(void) const;

    /** Casts the Field to int.
     * @return The inner data representation, i.e. bitfield. */
    operator int (void) const;
    
};

//**************************************************************************************************

#endif	/* _FIELD_H */

