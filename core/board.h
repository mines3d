/** ************************************************************************************************
 *  @file board.h
 *  
 *  @brief Playing 3D board.
 *  
 *  Provides access to playing elements and enables to control the game.
 *  
 *  @author Petr Kubiznak <kubiznak.petr@gmail.com>
 *  
 *  @since 2009-11-??
 *  
 **************************************************************************************************/

#ifndef _BOARD_H
#define	_BOARD_H

//**************************************************************************************************

#include <iostream>
#include "field.h"
using namespace std;

//**************************************************************************************************

// predefined game levels (board size and mines count)
static const int PRESETS[3][4] = {
	{ 3,  5,  5, 4 },                                                           //!< rookie (3 layers, 5 rows, 5 columns, 4 mines)
	{ 5,  8,  8, 20 },                                                          //!< advanced
	{ 8, 12, 12, 100 }                                                          //!< suicide
};

//**************************************************************************************************

/** Implements game board, provides access to game elements (fields). */
class Board {
protected:
	Field ***board;                                                             //!< Game board, i.e. 3d array of fields.
	int layers, rows, cols;                                                     //!< Size of game board.
	int minesCount;                                                             //!< Number of mines on the board.

	/** Puts mines on the board (call once).
	 * @return Zero on success, 1 on fail (too much mines to place on the board). */
	int placeMines(void);
	/** Sets number of mines in neighborhood of each field.
	 * @return Zero. */
	int numberBoard(void);
	/** Returns one field on specified position.
	 * @param layer z-coordinate
	 * @param row y-coordinate
	 * @param col x-coordinate
	 * @return Reference to the field. It can be modified (RW).
	 * @throws OutOfBoundException In case of invalid coordinates. */
	Field & getFieldRW(int layer, int row, int col) const;

public:
	/** Enum of all possible game modes (levels). */
	enum GameMode {ModeRookie=0, ModeAdvanced=1, ModeSuicide=2, ModeCustom=3};
	

	/** Creates new 3D game board.
	 * @param w Width of the board.
	 * @param h Height of the board.
	 * @param d Depth of the board.
	 * @param minesCount Number of mines to place on the board. */
	Board(int w, int h, int d, int minesCount);
	/** Destroys the board. */
	~Board(void);

	/** Returns number of layers of the board.
	 * @return Number of layers. */
	int getLayersCount(void) const;
	/** Returns number of rows of the board.
	 * @return Number of rows. */
	int getRowsCount(void) const;
	/** Returns number of columns of the board.
	 * @return Number of columns. */
	int getColsCount(void) const;
	/** Returns number of mines on the board.
	 * @return Total number of mines. */
	int getMinesCount(void) const;
	
	/** Returns one field on specified position.
	 * @param layer z-coordinate
	 * @param row y-coordinate
	 * @param col x-coordinate
	 * @return Reference to the field (read only).
	 * @throws OutOfBoundException In case of invalid coordinates. */
	const Field & getField(int layer, int row, int col) const;
	/** Returns one field on specified position.
	 * @param layer z-coordinate
	 * @param row y-coordinate
	 * @param col x-coordinate
	 * @return Reference to the field (read only).
	 * @throws OutOfBoundException In case of invalid coordinates. */
	const Field & operator () (int layer, int row, int col) const;
	
	/** Prints the whole game board CONTENT, i.e. uncovered (for debug use only). */
	void print(void) const;
	/* Prints the whole game board in the visible form.
	 * @param os Output stream.
	 * @param board Board to print.
	 * @return Given output stream. */
	friend ostream & operator << (ostream &os, const Board &board);
	
	/** Sets given field (un)marked.
	 * @param layer z-coordinate
	 * @param row y-coordinate
	 * @param col x-coordinate
	 * @param value Use true to mark, false to unmark given field.
	 * @throws OutOfBoundException In case of invalid coordinates. */
	void setFieldMark(int layer, int row, int col, bool value);
	
	/** Uncoveres given field and, if no mines are there and in the neigbourhood, uncoveres also
	 * the neigbourhood.
	 * @param layer z-coordinate
	 * @param row y-coordinate
	 * @param col x-coordinate
	 * @return True if mine is present on given field, false otherwise. */	
	bool uncover(int layer, int row, int col);
	/** Checks, whether all the not-mined fields are uncovered.
	 * @return True if no not-mined field remains, false otherwise. */
	bool isCleared(void) const;
	/** Returns number of fields MARKED as mined by the user.
	 * @return Number of marked fields. */
	int getMarkedCount(void) const;
	
	/** Returns game mode of this board.
	 * @return ModeRookie, ModeAdvanced, ModeSuicide or ModeCustom, depending on size of this board
	 *  and number of placed mines. */
	Board::GameMode getMode(void) const;
	/** Returns game mode of a board with given size and mines count.
	 * @param layers z-coordinate
	 * @param rows y-coordinate
	 * @param cols x-coordinate
	 * @param mines Number of mines placed at the board.
	 * @return ModeRookie, ModeAdvanced, ModeSuicide or ModeCustom, depending on size of this board
	 *  and number of placed mines. */
	static Board::GameMode getMode(int layers, int rows, int cols, int mines);
};

#endif	/* _BOARD_H */

