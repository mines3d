#include <pthread.h>
#include "../core/board.h"

#ifndef THREAD_CONT_H
#define THREAD_CONT_H

class Thread_controller
{
    public:
        
        int * selected;
        int * dim;
        int id;
        Board * b;
        
        pthread_t pthread1;
        pthread_mutex_t mutex;
        
        void (*ptf)(void);  
        void (*ptf_end)(void);
        
        Thread_controller(void);
        ~Thread_controller(void);  
        void cancel_glut_thread(void);
        void run_thread(void);
        void update_data(int x,int y,int z,Board * brd);
        void mutex_lock(void);
        void mutex_unlock(void);
        
        
};

#endif
