#include "thread_controller.h"
#include "../opengl/runGlutMainLoop.h"

Thread_controller::Thread_controller()
{

    selected = new int[6];
    dim = new int[3];
    id = 0;
    ptf = 0;
    ptf_end = 0;
    b = 0;
    pthread_mutex_init(&mutex, 0);
    
}

Thread_controller::~Thread_controller()
{
    delete[] selected;
    delete[] dim;
    selected = 0;
    dim = 0;
    ptf_end = 0;
    pthread_mutex_destroy(&mutex);    
}

void Thread_controller::run_thread(void)
{
    if (id == 0)
    {
        pthread_create(&pthread1,0,runGlutMainLoop,(void *)this);
        id = 1;
    }
    
}

void Thread_controller::cancel_glut_thread(void)
{
    pthread_mutex_lock(&mutex);
    
    ptf = 0;
    ptf_end();
    
    pthread_mutex_unlock(&mutex);
    
}

void Thread_controller::update_data(int x,int y,int z,Board * brd)
{
    pthread_mutex_lock(&mutex);
    b = brd;
    dim[0] = x;
    dim[1] = y;
    dim[2] = z;
    selected[0] = selected[1] = selected[2] = -1;
    selected[3] = selected[4] = selected[5] = -1;
    pthread_mutex_unlock(&mutex);
}

void Thread_controller::mutex_lock(void)
{
    pthread_mutex_lock(&mutex);
}

void Thread_controller::mutex_unlock(void)
{
    pthread_mutex_unlock(&mutex);
}
